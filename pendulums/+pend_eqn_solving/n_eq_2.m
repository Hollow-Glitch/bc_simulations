

function n_eq_2()
%     syms g   ...
%          m_1 ...
%          m_2 ...
%          l_1 ...
%          l_2 ...
%          theta_1 ...
%          theta_2 ...
%          theta_dot_1 ...
%          theta_dot_2 ...
%          theta_ddot_1 ...
%          theta_ddot_2 ;
     
     g            = sym('g');
     m_1          = sym('m_1');
     m_2          = sym('m_2');
     l_1          = sym('l_1');
     l_2          = sym('l_2');
     theta_1      = sym('theta_1');
     theta_2      = sym('theta_2');
     theta_dot_1  = sym('theta_dot_1');
     theta_dot_2  = sym('theta_dot_2');
     theta_ddot_1 = sym('theta_ddot_1');
     theta_ddot_2 = sym('theta_ddot_2');
    
     eq1_rhs_numerator = -m_2 * l_2 * theta_ddot_2 * cos(theta_1 - theta_2) ...
                         -m_2 * l_2 * theta_dot_2^2 * sin(theta_1 - theta_2) ...
                         -(m_1 + m_2) * g * sin(theta_1);
     eq1_rhs_denominator = (m_1 + m_2) * l_1;
     
     eq2_rhs_numerator = -l_1 * theta_ddot_1 * cos(theta_1 - theta_2) ...
                         +l_1 * theta_dot_1^2 * sin(theta_1 - theta_2) ...
                         -g * sin(theta_2);
     eq2_rhs_denominator = l_2;
     
     eq_1 = theta_ddot_1 == eq1_rhs_numerator / eq1_rhs_denominator;
     eq_2 = theta_ddot_2 == eq2_rhs_numerator / eq2_rhs_denominator;
     
     sys = [ eq_1 ; eq_2 ];
     
     utils_v2.display_sym_eq_in_latex(sys);
     
     sol = solve(sys, theta_ddot_1, theta_ddot_2);
     sol_arr = [sol.theta_ddot_1 ; sol.theta_ddot_2 ];
     
     utils_v2.display_sym_eq_in_latex(sol_arr);
end

