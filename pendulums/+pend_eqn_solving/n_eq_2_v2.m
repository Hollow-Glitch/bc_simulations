function out_sol_arr = n_eq_2_v2()
     g            = sym('g');
     m_1          = sym('m_1');
     m_2          = sym('m_2');
     l_1          = sym('l_1');
     l_2          = sym('l_2');
     theta_1      = sym('theta_1');
     theta_2      = sym('theta_2');
     theta_dot_1  = sym('theta_dot_1');
     theta_dot_2  = sym('theta_dot_2');
     theta_ddot_1 = sym('theta_ddot_1');
     theta_ddot_2 = sym('theta_ddot_2');
     
     eqn_1 = (m_1 + m_2) * l_1 * theta_ddot_1 ...
           + m_2 * l_2 * theta_ddot_2 * cos(theta_1 - theta_2) ...
           + m_2 * l_2 * theta_dot_2^2 * sin(theta_1 - theta_2) ...
           + (m_1 + m_2) * g * sin(theta_1) == 0;
       
     eqn_2 = m_2 * l_2^2 * theta_ddot_2 ...
           + m_2 * l_1 * l_2 * theta_ddot_1 * cos(theta_1 - theta_2) ...
           - m_2 * l_1 * l_2 * theta_dot_1 * sin(theta_1 - theta_2) * (theta_dot_1 - theta_dot_2) ...
           - m_2 * l_1 * l_2 * theta_dot_1 * theta_dot_2 * sin(theta_1 - theta_2) ...
           + m_2 * g * l_2 * sin(theta_2) == 0;
       
     sys = [ eqn_1 ; eqn_2 ];
     
     sol = solve(sys, theta_ddot_1, theta_ddot_2);
     sol_arr = [ sol.theta_ddot_1 ; sol.theta_ddot_2 ];
     
     utils_v2.display_sym_eq_in_latex(sys);
     utils_v2.display_sym_eq_in_latex(sol_arr);
     
     out_sol_arr = sol_arr;
end

