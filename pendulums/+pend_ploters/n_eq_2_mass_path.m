

function line_type = n_eq_2_mass_path(l_1, l_2, m_1, m_2, IC, tspan, ...
                                      x1_text_start, x1_text_end, x2_text_start, x2_text_end)
% line_type = n_eq_2_mass_path(l_1, l_2, m_1, m_2, IC)
% 
% initial conditions:
%     IC = [
%         theta_1;
%         theta_dot_1;
%         theta_2;
%         theta_dot_2
%         ];
% l_1, l_2  ...  string lengths
% m_1, m_2  ...  masses of the pendulum bobs
% tspan     ...  time span, ex.: [0 20]

    [t, dY] = ode45(@(t, y) model_pend_n_eq_2(t, y, l_1, l_2, m_1, m_2), tspan, IC);
    t = t;
    th_1     = dY(:, 1);
    th_dot_1 = dY(:, 2);
    th_2     = dY(:, 3);
    th_dot_2 = dY(:, 4);
    
    % transforming the polar coordinates into cartesian coordinates
    x_from_polar = @(l, theta) l .* cos(theta - pi/2); % reason for -pi/2 is that the transformation is considering phi to be measured from positive half axis x but we want it to be vertical so we rotate it by 90 degrees
    y_from_polar = @(l, theta) l .* sin(theta - pi/2);
    % m_1
    x_1 = x_from_polar(l_1, th_1);
    y_1 = y_from_polar(l_1, th_1);
    % m_2
    x_2 = x_1 + x_from_polar(l_2, th_2);
    y_2 = y_1 + y_from_polar(l_2, th_2);
    
    
    plot(x_1, y_1);
    hold on;
    line_type = plot(x_2, y_2);
    
%     cmap = autumn(length(t)); % colormap, with N colors
%     hold on;
%     for i = 1 : (length(t) -1)
%         line_type = plot([x_2(i), x_2(i+1)], [y_2(i), y_2(i+1)], 'color', cmap(i, :) );
%     end
    
    x1_text_start(x_1(1), y_1(1) )
    x1_text_end(x_1(length(t)), y_1(length(t)) );
    
    x2_text_start(x_2(1), y_2(1) )
    x2_text_end(x_2(length(t)), y_2(length(t)) );
    
    xlabel('x');
    ylabel('y');
end

