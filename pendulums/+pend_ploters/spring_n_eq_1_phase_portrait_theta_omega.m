function quiver_type = spring_n_eq_1_phase_portrait_theta_omega(k, m, l_I, theta_domain, omega_domain, r_I, v_I)
% n_eq_1_phase_plane(l)
% 
% k             ...  spring coeficient
% m             ...  pendulum bob mass
% l_I           ...  initial pendulum string length, radial displacement
% theta_domain  ...  angular displacement domain, ex:  -2.5 : 0.4 : 2.5
% omega_domain  ...  angular velocity domain, ex:    -5 : 0.4 : 5
% r_I      ...  radial displacement domain
% v_I      ...  radial velocity domain

    [theta_I, omega_I] = meshgrid(theta_domain, omega_domain);
    theta = zeros(size(theta_I) );
    omega = zeros(size(omega_I) );
    
%     [r_I, v_I] = meshgrid(r_domain, v_domain);
%     r = zeros(size(theta_I) );
%     v = zeros(size(omega_I) );
    
    % calculate the "vector" starting at [theta_I omega_I] and ending at [theta omega]
    for rowIndex = 1:length(omega_domain)
        for colIndex = 1:length(theta_domain)
            % using the MODEL
%             temp = model_pend_n_eq_1(0, ...
%                 [theta_I(rowIndex, colIndex), omega_I(rowIndex, colIndex)], l);
            IC = [
               r_I;    % initial radial displacement
               v_I;    % initial radial velocity
               theta_I(rowIndex, colIndex);    % initial angular displacement
               omega_I(rowIndex, colIndex)     % initial angular velocity
               ];
            temp = model_pend_spring_n_eq_1(0, IC, k, m, l_I);
            
            % r & v
            r(rowIndex, colIndex) = temp(1);
            v(rowIndex, colIndex) = temp(2);
            % theta & omega
            theta(rowIndex, colIndex) = temp(3);
            omega(rowIndex, colIndex) = temp(4);
        end
    end

    % draw phase portrait with vectors: from [theta_I omega_I] to [theta omega]
    quiver_type = quiver(theta_I, omega_I, theta, omega);
%     figure
%     quiver(r_I, v_I, r, v);
end

