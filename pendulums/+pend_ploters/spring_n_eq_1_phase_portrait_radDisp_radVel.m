function quiver_type = spring_n_eq_1_phase_portrait_radDisp_radVel(k, m, l_I, theta_I, omega_I, r_domain, v_domain)
% n_eq_1_phase_plane(l)
% 
% k          ...  spring coeficient
% m          ...  pendulum bob mass
% l_I        ...  initial pendulum string length, radial displacement
% theta_I    ...  initial angular displacement
% omega_I    ...  initial angular velocity
% r_domain   ...  radial displacement domain
% v_domain   ...  radial velocity domain

%     [theta_I, omega_I] = meshgrid(theta_domain, omega_domain);
%     theta = zeros(size(theta_I) );
%     omega = zeros(size(omega_I) );
    
    [r_I, v_I] = meshgrid(r_domain, v_domain);
    r = zeros(size(r_I) );
    v = zeros(size(v_I) );
    
    % calculate the "vector" starting at [theta_I omega_I] and ending at [theta omega]
    for rowIndex = 1:length(v_domain)
        for colIndex = 1:length(r_domain)
            % using the MODEL
            IC = [
               r_I(rowIndex, colIndex);    % initial radial displacement
               v_I(rowIndex, colIndex);    % initial radial velocity
               theta_I;    % initial angular displacement
               omega_I     % initial angular velocity
               ];
            temp = model_pend_spring_n_eq_1(0, IC, k, m, l_I);
            
            % r & v
            r(rowIndex, colIndex) = temp(1);
            v(rowIndex, colIndex) = temp(2);
            % theta & omega
%             theta(rowIndex, colIndex) = temp(3);
%             omega(rowIndex, colIndex) = temp(4);
        end
    end

    % draw phase portrait with vectors: from [theta_I omega_I] to [theta omega]
%     quiver(theta_I, omega_I, theta, omega);
% %     figure
    quiver_type = quiver(r_I, v_I, r, v);
end