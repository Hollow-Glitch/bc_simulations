

function line_type = n_eq_2_plot(l_1, l_2, m_1, m_2, IC, tspan)
% line_type = n_eq_2_mass_path(l_1, l_2, m_1, m_2, IC)
% 
% initial conditions:
%     IC = [
%         theta_1;
%         theta_dot_1;
%         theta_2;
%         theta_dot_2
%         ];
% l_1, l_2  ...  string lengths
% m_1, m_2  ...  masses of the pendulum bobs
% tspan     ...  time span, ex.: [0 20]

    [t, dY] = ode45(@(t, y) model_pend_n_eq_2(t, y, l_1, l_2, m_1, m_2), tspan, IC);
    t = t;
    th_1     = dY(:, 1);
    th_dot_1 = dY(:, 2);
    th_2     = dY(:, 3);
    th_dot_2 = dY(:, 4);
    
    plot(t, wrapToPi(th_1), 'DisplayName', '\theta_1(t)');
    hold on;
    line_type = plot(t, wrapToPi(th_2), 'DisplayName', '\theta_2(t)');
    legend('Location', 'northoutside', 'AutoUpdate', 'off', 'NumColumns', 2);
    xline(0);
    yline(0);
    
%     set(gca,'TickLabelInterpreter', 'latex');
%     yticks([-pi, -pi/2, 0, pi/2, pi]);
%     yticklabels({'\small $-\pi$', '\large $-\frac{\pi}{2}$', '$0$', '$\frac{\pi}{2}$', '$\pi$', });
%     set(gca,'YTick',[],'FontSize',24)

%     set(gca,'YTick', [-pi, -pi/2, 0, pi/2, pi], ...
%         'YTickLabel',{'$-\pi$', '$-\frac{\pi}{2}$', '$0$', '$\frac{\pi}{2}$', '$\pi$'}, ...
%         'FontSize',24)

    % 
    set(gca,'YTick', [-pi, -pi/2, 0, pi/2, pi], ...
        'YTickLabel',{'\fontsize{15}-\pi', '\fontsize{15}-\pi/\fontsize{12}2', '\fontsize{15}0', '\fontsize{15}\pi/\fontsize{12}2', '\fontsize{15}\pi'})


%     xlabel('t');
%     ylabel({'\theta_1(t)'; '\theta_2(t)'}, 'Rotation', 0, 'VerticalAlignment', 'middle', 'HorizontalAlignment', 'right');
end

