function quiver_type = n_eq_1_phase_portrait(l, theta_domain, omega_domain)
% n_eq_1_phase_plane(l)
% 
% l             ...  pendulum string length
% theta_domain  ...  ex:  -2.5 : 0.4 : 2.5
% omega_domain  ...  ex:    -5 : 0.4 : 5

    % we want a grid where 
    % - x is from theta_domain
    % - y is from omega_domain
%     theta_domain = -2.5 : 0.4 : 2.5;
%     omega_domain = -5 : 0.4 : 5;
    [theta_I, omega_I] = meshgrid(theta_domain, omega_domain);

    % preallocate theta and omega
    % - note, meshgrid size and hence the size of theta & omega
    %   is in an "inverse relationship" to theta_domain & omega_domain
    %   length, meaning that there are as many columns as elements in 
    %   theta_domain, and as many rows as omega_domain
    theta = zeros(size(theta_I) );
    omega = zeros(size(omega_I) );
    
    % calculate the "vector" starting at [theta_I omega_I] and ending at [theta omega]
    for rowIndex = 1:length(omega_domain)
        for colIndex = 1:length(theta_domain)
            % using the MODEL for PEND_n_EQ_1
            temp = model_pend_n_eq_1(0, ...
                [theta_I(rowIndex, colIndex), omega_I(rowIndex, colIndex)], l);
            % theta & omega
            theta(rowIndex, colIndex) = temp(1);
            omega(rowIndex, colIndex) = temp(2);
        end
    end

    % draw phase portrait with vectors: from [theta_I omega_I] to [theta omega]
    quiver_type = quiver(theta_I, omega_I, theta, omega);
end

