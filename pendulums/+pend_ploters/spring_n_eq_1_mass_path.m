

function line_type = spring_n_eq_1_mass_path(k, m, l_I, tspan,       ...
                                             x_rI, x_aI, v_rI, v_aI, ...
                                             text_start, text_end)
    IC = [
        x_rI;
        v_rI;
        x_aI;
        v_aI
        ];
    [t, Y] = ode45(@(t,y) model_pend_spring_n_eq_1(t,y, k, m, l_I), tspan, IC);
    x_r = Y(:,1);  % radial displacement
    v_r = Y(:,2);  % radial velocity
    x_a = Y(:,3);  % angular displacement
    v_a = Y(:,4);  % angular velocity

    % transforming the polar coordinates into cartesian coordinates
    x = (l_I + x_r).*cos(x_a-pi/2); % reason for -pi/2 is that the transformation is considering phi to be measured from positive half axis x but we want it to be vertical so we rotate it by 90 degrees
    y = (l_I + x_r).*sin(x_a-pi/2);
    
    cmap = parula(length(t)); % colormap, with N colors
    hold on;
    for i = 1 : (length(t) -1)
        line_type = plot([x(i), x(i+1)], [y(i), y(i+1)], 'color', cmap(i, :) );
    end
%     text(x(1), y(1)+0.1, 'Start \rightarrow', 'HorizontalAlignment','right');
%     text(x(length(t)), y(length(t)), '\leftarrow End')
    text_start(x(1), y(1) )
    text_end(x(length(t)), y(length(t)) );
    xlabel('x');
    ylabel('y');
end

