
function line_type = spring_n_eq_1_plot(k, m, l_I, tspan, x_rI, x_aI, v_rI, v_aI)
%     k = 3;
%     m = 0.25;
%     l_I = 1;
%     x_rI = 0;
%     x_aI = pi/4;
%     v_rI = 0;
%     v_aI = 0;
    IC = [
        x_rI;
        v_rI;
        x_aI;
        v_aI
        ];
    [t, Y] = ode45(@(t,y) model_pend_spring_n_eq_1(t,y, k, m, l_I), tspan, IC);
    x_r = Y(:,1);  % radial displacement
    v_r = Y(:,2);  % radial velocity
    x_a = Y(:,3);  % angular displacement
    v_a = Y(:,4);  % angular velocity
    
    plot(t, x_r, 'DisplayName', 'r(t)');
    hold on;
    line_type = plot(t, x_a, 'DisplayName', '\theta(t)');
    legend('Location', 'northoutside', 'AutoUpdate', 'off', 'NumColumns', 2);
    xline(0);
    yline(0);
    xlabel('t');
    ylabel({'r(t)'; '\theta(t)'}, 'Rotation', 0, 'VerticalAlignment', 'middle', 'HorizontalAlignment', 'right');
end

