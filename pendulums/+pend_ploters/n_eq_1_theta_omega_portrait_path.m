

function line_type = n_eq_1_theta_omega_portrait_path(l, tspan, IC)
% draw_theta_omega_circle(l, IC)
% 
% l      ...  pendulum string length
% tspan  ...  time span for ode45, ex: [0 10]
% IC     ...  row vector
%        ...  IC(1) is theta_I  while  IC(2) is omega_I
% 
% Suggestion:
% - use with n_eq_1_phase_portrait() as such:

    [t, sol] = ode45(@(t, y) model_pend_n_eq_1(t, y, l), tspan, IC);
    t = t;
    theta = sol(:, 1);
    omega = sol(:, 2);
    line_type = plot(theta, omega);
end
