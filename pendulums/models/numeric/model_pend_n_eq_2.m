
function dY = model_pend_n_eq_2(t, IC, l_1, l_2, m_1, m_2)
% dY = model_pend_n_eq_2(t, IC, l_1, l_2, m_1, m_2)
% 
% initial conditions:
%     IC = [
%         theta_1;
%         theta_dot_1;
%         theta_2;
%         theta_dot_2
%         ];
% l_1, l_2  ...  string lengths
% m_1, m_2  ...  masses of the pendulum bobs
    
    g = 9.81;


    q_1 = @(th_1, th_2) ((m_2 * l_2) / ((m_1 + m_2) * l_1)) * cos(th_1 - th_2);
    
    q_2 = @(th_1, th_2) (l_1 / l_2) * cos(th_1 - th_2);
    
    f_1 = @(th_1, th_2, dth_1, dth_2) ...
        - ( (m_2 * l_2) / ( (m_1 + m_2)*l_1 ) ) * dth_2^2 * sin(th_1 - th_2) ...
        - (g/l_1) * sin(th_1);
    
    f_2 = @(th_1, th_2, dth_1, dth_2) ...
        (l_1 / l_2) * dth_1^2 * sin(th_1 - th_2) ...
        - (g/l_2)*sin(th_2);
    
    y_1 = IC(1);
    y_2 = IC(2);
    y_3 = IC(3);
    y_4 = IC(4);
    
    q_1 = q_1(y_1, y_3);
    q_2 = q_2(y_1, y_3);
    f_1 = f_1(y_1, y_3, y_2, y_4);
    f_2 = f_2(y_1, y_3, y_2, y_4);
    
    dY = [
            y_2;
            (f_1 - q_1 * f_2 ) / (1 - q_1 * q_2);
            y_4;
            (f_2 - q_2 * f_1 ) / (1 - q_1 * q_2)
        ];
end

