function dY = model_pend_n_eq_1(t, IC, l,   varargin)
% analytic_pend_n_eq_1_small_osc(t, g, l, theta_I, varargin)
% 
% mandatory variables:
% l       ... pendulum string length
% IC      ... initial conditions
% 
% Optional variables:
% g       ... gravitational acceleration
%         ... type: scalar & numeric; default value: 9.81

    p = inputParser();
    p.addOptional('return_api', false, @(x) islogical(x) && isscalar(x) );
    p.addOptional('g',          9.81, @(x) isnumeric(x) && isscalar(x) );
    p.parse(varargin{:});
    args = p.Results;
    g = args.g;

    y1 = IC(1);
    y2 = IC(2);
    
    dy1 = y2;
    dy2 = -(g/l) * sin(y1);
    
    dY = [
        dy1;
        dy2
        ];
end

