function dY = triple_pendulum_simplified_model(t, Y, A, g)
% A ... array of lengths

    dy1 = Y(2);
    dy2 = g * ( 2 * ( (Y(3) - Y(1))/A(2) - Y(1)/A(1)) - Y(1)/A(1) );
    dy3 = Y(4);
    dy4 = g * ( 1 * ( (Y(5) - Y(3))/A(3) - (Y(3)-Y(1))/A(3) ) - (Y(3)-Y(1))/A(2) );
    dy5 = Y(6);
    dy6 = g * ( - (Y(5)-Y(3))/A(3) );
    
    dY = [
        dy1;
        dy2;
        dy3;
        dy4;
        dy5;
        dy6;
        ];
end

