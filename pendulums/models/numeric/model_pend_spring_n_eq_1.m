function dY = model_pend_spring_n_eq_1(t, IC, k, m, l_I)
% dY = model_pend_spring_n_eq_1(t, IC, k, m, l_I)
%
% return:
% x_r = dY(:,1);  % radial velocity 
% v_r = dY(:,2);  % radial acceleration
% x_a = dY(:,3);  % angular velocity
% v_a = dY(:,4);  % angular acceleration
%
% IC = [
%    x_rI;    ... initial radial displacement
%    v_rI;    ... initial radial velocity
%    x_aI;    ... initial angular displacement
%    v_aI     ... initial angular velocity
%    ];
%
% k   ... spring constant
% m   ... mass
% l_I ... initial spring length
% ! gravitation is present in the system: g = 9.81
    os = k / m;
    g = 9.81;


    y1 = IC(1);
    y2 = IC(2);
    y3 = IC(3);
    y4 = IC(4);
    
    dy1 = y2;
    dy2 = (l_I + y1) * y4^2 - os*y1 + g*cos(y3);
    dy3 = y4;
    dy4 = -(2*y2*y4)/(l_I + y1) - (g*sin(y3))/(l_I + y1);
    
    dY = [dy1;dy2;dy3;dy4];
end

