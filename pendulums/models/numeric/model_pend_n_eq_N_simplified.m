

function dY = model_pend_n_eq_N_simplified(t, Y, n, A, g)
% A ... array of lengths

    y_i1 = Y(1:2:2*n);
    y_i2 = Y(2:2:2*n);
    
    dy_i1 = zeros(1, n);
    dy_i2 = zeros(1, n);
    
    dy_i1(1) = y_i2(1);
    dy_i2(1) = g * (...
            (n-1) * ( (y_i1(2) - y_i1(1) ) / A(2) - y_i1(1) / A(1) ) ...
            - y_i1(1) / A(1) ...
        );
    
    for i = 2:(n-1)
        dy_i1(i) = y_i2(i);
        dy_i2(i) = g*(...
            (n-i) * ( (y_i1(i+1) - y_i1(i))/A(i+1) - (y_i1(i) - y_i1(i-1))/A(i) ) ...
            - (y_i1(i) - y_i1(i-1))/A(i) ...
        );
    end
    
    dy_i1(n) = y_i2(n);
    dy_i2(n) = g * ( -(y_i1(n) - y_i1(n-1))/ A(n) );
    
    dY = zeros(2*n, 1);
    dY(1:2:2*n) = dy_i1;
    dY(2:2:2*n) = dy_i2;
end

