function out = analytic_pend_n_eq_1_small_osc(t, l, theta_I,   varargin)
% analytic_pend_n_eq_1_small_osc(t, g, l, theta_I, varargin)
% 
% mandatory variables:
% t       ... time
% l       ... pendulum string length
% theta_I ... initial angular displacement
% 
% Optional variables:
% g       ... gravitational acceleration
%         ... type: scalar & numeric; default value: 9.81

    p = inputParser();
    p.addOptional('return_api', false, @(x) islogical(x) && isscalar(x) );
    p.addOptional('g',          9.81, @(x) isnumeric(x) && isscalar(x) );
    p.parse(varargin{:});
    args = p.Results;
%     api = cell2struct(p.Parameters, p.Parameters, 2); % the third parameter for returning the api always must be '2'
%     
%     if args.return_api
%         out = api;
%         return;
%     end

    %% computations

    g = args.g;
    omega = sqrt(g / l);
    out = l .* theta_I .* cos(omega .* t);
end

