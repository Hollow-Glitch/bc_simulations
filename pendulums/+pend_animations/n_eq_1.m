

function n_eq_1()
% Animation of the: Simple pendulum case (no damping)

    g = 9.81;
    l = 1;
    theta_I = pi/4;
    v_I = 0;
    IC = [theta_I;
        v_I];
    [t,Y] = ode45(@(t,y) model_pend_n_eq_1(t, y, l), [0 10], IC);
    t = t;
    theta = Y(:, 1);
    v = Y(:, 2);
    
    R = @(theta) [ cos(theta), -sin(theta); sin(theta), cos(theta) ];
    C = [0, 1];   % ceiling
    el = [0, -1]; % equilibrium line
    
%     subp = @(a) subplot(3, 2, a);
%     animplot = @() subp([3 4 5 6]);
%     
%     subp(1);
%     plot(t, wrapToPi(theta), 'DisplayName', '\theta_1(t)');
%     xline(0);
%     yline(0);
%     
%     subp(2);
%     plot(t, wrapToPi(v), 'DisplayName', '\theta_1(t)');
%     xline(0);
%     yline(0);
%     
%     animplot();
    xlim([-1.5, 1.5]);
    ylim([-0.5, 1.5]);
    xlabel('x', 'Interpreter', 'latex');
    ylabel('y', 'Interpreter', 'latex');
    title('Simple pendulum', 'Interpreter', 'latex');
    
    pointMass = @(C) rectangle('Position',[C(1)-0.05, C(2)-0.05, .1, .1],'FaceColor',[0 0 0]);
    for i = 1:length(t)
        cla;
        %viscircles(C, 1);
        xline(0);
        yline(1);
        yline(0);
        
        sv = R(theta(i)) * (el'); % string vector
        mp = C' + sv; % mass position
        % VERY BIG SURPRISE, ADDING TOGETHER A ROW AND A COLUMN
        % VECTOR IS NOT AN ERROR, AND IF C and sv are not both
        % column or row vectors this looks like as if the string was
        % changing its length
        lineBetweenTwoPoints(C, mp);
        pointMass(mp);
        
%         pause(0.05);
        pause(0.02);
    end
end
