

function n_eq_2()
% Animation of the: Double pendulum case (no damping)

    l_1 = 1;
    l_2 = 1;
    m_1 = 1;
    m_2 = 2;
    IC = [
        pi;
        0;
        pi/2;
        0
        ];
    [t, dY] = ode45(@(t, y) model_pend_n_eq_2(t, y, l_1, l_2, m_1, m_2), [0 40], IC);
    t = t;
    th_1     = dY(:, 1);
    th_dot_1 = dY(:, 2);
    th_2     = dY(:, 3);
    th_dot_2 = dY(:, 4);
    
    % transforming the polar coordinates into cartesian coordinates
    x_from_polar = @(l, theta) l .* cos(theta - pi/2); % reason for -pi/2 is that the transformation is considering phi to be measured from positive half axis x but we want it to be vertical so we rotate it by 90 degrees
    y_from_polar = @(l, theta) l .* sin(theta - pi/2);
    % m_1
    x_1 = x_from_polar(l_1, th_1);
    y_1 = y_from_polar(l_1, th_1);
    % m_2
    x_2 = x_1 + x_from_polar(l_2, th_2);
    y_2 = y_1 + y_from_polar(l_2, th_2);
    
    xlim([ min([x_1;x_2]), max([x_1;x_2]) ]);
    ylim([ min([0;y_1;y_2]), max([0;y_1;y_2]) ]);
    xlabel('x', 'Interpreter', 'latex');
    ylabel('y', 'Interpreter', 'latex');
    title('Double pendulum', 'Interpreter', 'latex');
    hold on;
    pointMass = @(C) rectangle('Position',[C(1)-0.05, C(2)-0.05, .1, .1],'FaceColor',[0 0 0]);
    for i = 1:length(t)
        cla;
        
        line([0, x_1(i)], [0, y_1(i)]);
        line([x_1(i), x_2(i)], [y_1(i), y_2(i)]);
        pointMass([x_1(i), y_1(i)]);
        pointMass([x_2(i), y_2(i)]);
        
        pause(0.01);
    end
end
