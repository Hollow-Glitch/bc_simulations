

function n_eq_N_simplified()
% Animation of the: approximated N pendulum case (no damping)

    n = 10;         % pendulum count
    g = 9.81;       % gravitational acceleration
    A = ones(1,n);  % length of pendulum strings
    
    % Initial conditions
    x_i = zeros(1,n);
    v_i = zeros(1,n);
    x_i(n-1) = 0.05;
    temp = [x_i; v_i];
    IC = temp(:);

    [t,Y] = ode45(@(t,y) model_pend_n_eq_N_simplified(t, y, n, A, g), [0 10], IC);
    t = t;
    x_i = Y(:, 1:2:2*n);
    v_i = Y(:, 2:2:2*n);
    
    R = @(theta) [ cos(theta), -sin(theta); sin(theta), cos(theta) ];
    C = [0, 1];   % ceiling
    el = [0, -1]; % equilibrium line
    
    mp_j = zeros(2,n);
    sv_j = zeros(2,n);
    
    xlim([-0.25, 0.25]);
    ylim([-sum(A), 1.2]);
    xlabel('x', 'Interpreter', 'latex');
    ylabel('y', 'Interpreter', 'latex');
    title('\(N\) pendulum', 'Interpreter', 'latex');
    
    pointMassWidth = 1/100;
    pointMassHeight = 1/10;
%     pointMass = @(C) rectangle('Position',[C(1)-0.05, C(2)-0.05, .1, .1],'FaceColor',[0 0 0]);
    pointMass = @(C) rectangle('Position',[C(1)-pointMassWidth/2, C(2)-pointMassHeight/2, pointMassWidth, pointMassHeight],'FaceColor',[0 0 0]);
    for i = 1:length(t)
        cla
        
        xline(0);
        
        theta_j(1:n) = asin(x_i(i, 1:n) ./ A(1:n));
        for j = 1:n
            sv_j(:, j) = R(theta_j(j)) * (el');
        end
        
        mp_j(:, 1) = C' + sv_j(:, 1);
        for j = 2:n
            mp_j(:, j) = mp_j(:, j-1) + sv_j(:, j);
        end
        
%         for j = 1:n
%             viscircles(mp_j(:,j)', 0.05);
%         end
        for j = 1:n
            pointMass(mp_j(:,j));
        end
        
        lineBetweenTwoPoints(C, mp_j(:, 1));
        for j = 2:n
            lineBetweenTwoPoints(mp_j(:, j-1), mp_j(:, j));
        end
            
        pause(0.01);
    end
end
