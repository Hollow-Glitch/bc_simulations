

function n_eq_1_spring()
% Animation of the: Spring pendulum case (no damping)

    k = 1;
    m = 0.25;
    l_I = 1;
    x_rI = 0.1;
    x_aI = pi/4;
    v_rI = 0;
    v_aI = 0;
    IC = [
        x_rI;
        v_rI;
        x_aI;
        v_aI
        ];
    [t, Y] = ode45(@(t,y) model_pend_spring_n_eq_1(t,y, k, m, l_I), [0 20], IC);
    x_r = Y(:,1);  % radial displacement
    v_r = Y(:,2);  % radial velocity
    x_a = Y(:,3);  % angular displacement
    v_a = Y(:,4);  % angular velocity
    
    spring = Spring(0.1, 5);
    
    % transforming the polar coordinates into cartesian coordinates
    o = [0 0];
    x = (l_I + x_r).*cos(x_a-pi/2); % reason for -pi/2 is that the transformation is considering phi to be measured from positive half axis x but we want it to be vertical so we rotate it by 90 degrees
    y = (l_I + x_r).*sin(x_a-pi/2);
    [x_r, x_a, x, y]
    
    xlim([min(x), max(x)]);
    ylim([min(y)-1, 0]);
    xlabel('x', 'Interpreter', 'latex');
    ylabel('y', 'Interpreter', 'latex');
    title('Spring pendulum', 'Interpreter', 'latex');
    hold on;
    pointMass = @(C) rectangle('Position',[C(1)-0.05, C(2)-0.05, .05, .2],'FaceColor',[0 0 0]);
    for i = 1:length(t)
        cla;
        
        plot(x(1:i), y(1:i),  'MarkerSize', 5);
        [springX, springY] = spring.getSpr(o, [x(i), y(i)]);
        plot(springX, springY);
        pointMass([x(i), y(i)]);
        
        % drawnow;
        pause(0.01);
    end
end
