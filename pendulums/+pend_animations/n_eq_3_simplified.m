

function n_eq_3_simplified()
% Animation of the: approximated Triple pendulum case (no damping)

    g = 9.81;
    A = [1, 1, 1];
    x1 = 0;
    v1 = 0;
    x2 = 0;
    v2 = 0;
    x3 = 0.2;
    v3 = 0;
    IC = [x1; v1; x2; v2; x3; v3];
    [t,Y] = ode45(@(t,y) model_pend_n_eq_3_simplified(t, y,A, g), [0 10], IC);
    t = t;
    x_i = Y(:, 1:2:6);
    v_i = Y(:, 2:2:6);
    
    R = @(theta) [ cos(theta), -sin(theta); sin(theta), cos(theta) ];
    C = [0, 1];   % ceiling
    el = [0, -1]; % equilibrium line
    
    xlim([-1.5, 1.5]);
    ylim([-4, 1.5]);
    xlabel('x', 'Interpreter', 'latex');
    ylabel('y', 'Interpreter', 'latex');
    title('Double pendulum', 'Interpreter', 'latex');
    
    for i = 1:length(t)
        cla;
        
        xline(0);
        yline(1);
        yline(0);
        
        theta_j(1:3) = asin(x_i(i, 1:3)./ A(1:3));   
        sv_1 = R(theta_j(1)) * (el');
        sv_2 = R(theta_j(2)) * (el');
        sv_3 = R(theta_j(3)) * (el');
        mp_1 = C' + sv_1;
        mp_2 = mp_1 + sv_2;
        mp_3 = mp_2 + sv_3;
        
        lineBetweenTwoPoints(C, mp_1);
        lineBetweenTwoPoints(mp_1, mp_2);
        lineBetweenTwoPoints(mp_2, mp_3);
        
        pause(0.1);
    end
end