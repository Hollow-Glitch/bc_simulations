

function n_eq_1_phase_portrait()
%     close all;
%     draw_circular(l);
%     
%     close all;
%     draw_non_circular(l);
    
    close all;
    draw_proper(3);
end

function draw_proper(l)
    theta_domain = -4 : 0.4 : 10;
    omega_domain = -5 : 0.4 : 5;
    tspan = [0 12];

    pend_ploters.n_eq_1_phase_portrait(l, theta_domain, omega_domain);
    hold on;
    
    pend_ploters.n_eq_1_theta_omega_portrait_path(l, tspan, [2.8, 0]);
    pend_ploters.n_eq_1_theta_omega_portrait_path(l, tspan, [3.6, 0]);
    pend_ploters.n_eq_1_theta_omega_portrait_path(l, [0 40], [3.2, 0]);

    % left inter circles
    pend_ploters.n_eq_1_theta_omega_portrait_path(l, tspan, [0.2,0]);
    pend_ploters.n_eq_1_theta_omega_portrait_path(l, tspan, [1,0]);
    pend_ploters.n_eq_1_theta_omega_portrait_path(l, tspan, [2,0]);
    
    % right inter circles
    pend_ploters.n_eq_1_theta_omega_portrait_path(l, tspan, [7,0]);
    pend_ploters.n_eq_1_theta_omega_portrait_path(l, tspan, [8,0]);
    
    % waves above
    pend_ploters.n_eq_1_theta_omega_portrait_path(l, tspan, [-4,2]);
    pend_ploters.n_eq_1_theta_omega_portrait_path(l, tspan, [-4,3]);
    pend_ploters.n_eq_1_theta_omega_portrait_path(l, tspan, [-4,4]);
    
    % waves belov
    pend_ploters.n_eq_1_theta_omega_portrait_path(l, tspan, [10,-2]);
    pend_ploters.n_eq_1_theta_omega_portrait_path(l, tspan, [10,-3]);
    fig = pend_ploters.n_eq_1_theta_omega_portrait_path(l, tspan, [10,-4]);

    % stylizing
    utils_v2.figure_decorators.bc_style_squareish(fig, 'width_multiplier', 2);
    xlim([min(theta_domain), max(theta_domain)]);
    
    name = sprintf('pend_n=1_phase_portrait__g=%f_l=%f', 9.81, l );
    fprintf("saving figure to file with name: %s\n", name);
    utils_v2.smart_plot_print(fig, name);
end
