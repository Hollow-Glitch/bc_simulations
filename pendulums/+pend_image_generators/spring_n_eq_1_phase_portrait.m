

function spring_n_eq_1_phase_portrait()
    close all;
    % phase portrait theta vs theta'
    theta_domain = -2.5 : 0.4 : 2.5;
    omega_domain = -5 : 0.4 : 5;
    r_I = 2;
    v_I = 0;
    pend_ploters.spring_n_eq_1_phase_portrait_theta_omega(1,1,2, ...
        theta_domain, omega_domain, r_I, v_I);
    
    close all;
    % phase portrait r vs r'
    theta_I = 0.5;
    omega_I = 0;
    r_domain = -2.5 : 1 : 20;
    v_domain = -5 : 0.5 : 5;
    pend_ploters.spring_n_eq_1_phase_portrait_radDisp_radVel(1,1,2, ...
        theta_I, omega_I, r_domain, v_domain);
end

