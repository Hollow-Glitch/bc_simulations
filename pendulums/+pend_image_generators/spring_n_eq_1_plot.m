

function spring_n_eq_1_plot()
    k = 1;
    m = 0.25;
    l_I = 1;
    tspan = [0 20];
    x_rI = 0.1;
    x_aI = pi/4;
    v_rI = 0;
    v_aI = 0;
    ID = 0;
    
    text_start = @(x, y) text(x+0, y+0.1, 'Start \rightarrow', 'HorizontalAlignment','right');
    text_end   = @(x, y) text(x+0, y+0, '\leftarrow End');
    
    close all;
    fig_1 = pend_ploters.spring_n_eq_1_plot(k, m, l_I, tspan, x_rI, x_aI, v_rI, v_aI);
    utils_v2.figure_decorators.bc_style_squareish(fig_1, 'width_multiplier', 1);
    name = sprintf('spring_pend_n=1_theta(t)_r(t)_plot_ID=%d', ID);
    fprintf("saving figure to file with name: %s\n", name);
    utils_v2.smart_plot_print(fig_1, name);
    
    close all;
    fig_2 = pend_ploters.spring_n_eq_1_mass_path(k, m, l_I, tspan, x_rI, x_aI, v_rI, v_aI, ...
                                                 text_start, text_end);
    utils_v2.figure_decorators.bc_style_squareish(fig_2, 'width_multiplier', 1);
    name = sprintf('spring_pend_n=1_mass_path_ID=%d', ID);
    fprintf("saving figure to file with name: %s\n", name);
    utils_v2.smart_plot_print(fig_2, name);
    
    %%
    k = 0.5;
    m = 0.25;
    l_I = 1;    
    tspan = [0 40];
    x_rI = 0.1;
    x_aI = pi/4;
    v_rI = 0;
    v_aI = 0;
    ID = 1;
    
    text_start = @(x, y) text(x+0, y+0.25, 'Start \rightarrow', 'Rotation', -5, 'HorizontalAlignment','right');
    text_end   = @(x, y) text(x+0, y+0.25, '\leftarrow End', 'Rotation', 7);
    
    close all;
    fig_1 = pend_ploters.spring_n_eq_1_plot(k, m, l_I, tspan, x_rI, x_aI, v_rI, v_aI);
    utils_v2.figure_decorators.bc_style_squareish(fig_1, 'width_multiplier', 1);
    name = sprintf('spring_pend_n=1_theta(t)_r(t)_plot_ID=%d', ID);
    fprintf("saving figure to file with name: %s\n", name);
    utils_v2.smart_plot_print(fig_1, name);
    
    close all;
    fig_2 = pend_ploters.spring_n_eq_1_mass_path(k, m, l_I, tspan, x_rI, x_aI, v_rI, v_aI, ...
                                                 text_start, text_end);
    utils_v2.figure_decorators.bc_style_squareish(fig_2, 'width_multiplier', 1);
    name = sprintf('spring_pend_n=1_mass_path_ID=%d', ID);
    fprintf("saving figure to file with name: %s\n", name);
    utils_v2.smart_plot_print(fig_2, name);
    
    %%
    k = 2;
    m = 0.25;
    l_I = 1;
    tspan = [0 20];
    x_rI = 0.1;
    x_aI = pi/4;
    v_rI = 0;
    v_aI = 0;
    ID = 2;

    text_start = @(x, y) text(x+0, y+0.1, 'Start \rightarrow', 'HorizontalAlignment','right');
    text_end   = @(x, y) text(x+0, y+0, '\leftarrow End');
    
    close all;
    fig_1 = pend_ploters.spring_n_eq_1_plot(k, m, l_I, tspan, x_rI, x_aI, v_rI, v_aI);
    utils_v2.figure_decorators.bc_style_squareish(fig_1, 'width_multiplier', 1);
    name = sprintf('spring_pend_n=1_theta(t)_r(t)_plot_ID=%d', ID);
    fprintf("saving figure to file with name: %s\n", name);
    utils_v2.smart_plot_print(fig_1, name);
    
    close all;
    fig_2 = pend_ploters.spring_n_eq_1_mass_path(k, m, l_I, tspan, x_rI, x_aI, v_rI, v_aI, ...
                                                 text_start, text_end);
    utils_v2.figure_decorators.bc_style_squareish(fig_2, 'width_multiplier', 1);
    name = sprintf('spring_pend_n=1_mass_path_ID=%d', ID);
    fprintf("saving figure to file with name: %s\n", name);
    utils_v2.smart_plot_print(fig_2, name);
end

