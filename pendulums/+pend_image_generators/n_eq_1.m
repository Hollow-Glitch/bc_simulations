
function n_eq_1(theta_I, varargin)
% n_eq_1(theta_I)
% theta_I  ...  initial angular displacement
% 
% Optional variables:
% g       ... gravitational acceleration
%         ... type: scalar & numeric; default value: 9.81

    %% parsing
    p = inputParser();
    p.addOptional('return_api', false, @(x) islogical(x) && isscalar(x) );
    p.addOptional('g',          9.81, @(x) isnumeric(x) && isscalar(x) );
    p.parse(varargin{:});
    args = p.Results;
    g = args.g;
    
    %% setup
    t_start = 0;
    t_end = 10;
    IC = [theta_I;
          0];
    l = 1;
    
    %% analytical
    t_ana = t_start:0.1:t_end;
    theta_ana = analytic_pend_n_eq_1_small_osc(t_start:0.1:t_end, l, IC(1), 'g', g);
    
    %% numerical
    [t_num, Y_num] = ode45(@(t,y) model_pend_n_eq_1(t, y, l, 'g', g), [t_start t_end], IC);
    theta_num = Y_num(:, 1);
    % omega_num = Y_num(:, 2); % not needed
    
    %% ploting both
    plot(t_ana, theta_ana);
    hold on;
    plot(t_num, theta_num);
    legend("\theta_{ana}(t)","\theta_{num}(t)", 'Location', 'northoutside', 'NumColumns', 2, 'AutoUpdate', 'off');
    ylabel("\theta");
    xlabel("t");
    yline(0);
    ylim([min(theta_num)*1.1, max(theta_num)*1.1]);
    
%     name = sprintf('pend_n=1_comparison_ana_vs_num__g=%f_l=%f_IC=[%s]', g, l, IC);
    name = sprintf('pend_n=1_comparison_ana_vs_num__g=%f_l=%f_theta_I=%f', g, l, IC(1) );
    fprintf("saving file with name: %s", name);
    smart_plot_print(name);
end