function text_type = display_sym_eq_in_latex(sym_eq)
% display_sym_eq_in_latex(sym_eq)
% 
% arguments:
% sym_eq  ...  symbolic equation

    as_latex = latex(sym_eq);
    as_out_latex = ['$$' as_latex '$$'];

    fig = figure;
    fig.Color = 'white';
    a = gca;
    a.OuterPosition = [ 0 0 0 0 ];
    axis off;
    
    t = text('Units', 'pixels', ...
         'VerticalAlignment', 'bottom', ...
         'fontsize', 14, ...
         'color', 'k', ...
         'interpreter', 'latex', ...
         'string', as_out_latex ...
    );

    margin = 32;
    
    t.Position(1) = t.Position(1) + margin;
    t.Position(2) = t.Position(2) + margin;
    
    text_width = t.Extent(3);
    text_height = t.Extent(4);
    fig.Position(3) = text_width + 2*margin;
    fig.Position(4) = text_height + 2*margin;

    text_type = t;
end

