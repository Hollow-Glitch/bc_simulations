

function n_eq_2()
% n_eq_2 - Animation of the: Two Coupled Oscillators case (no damping)

    k1   = 0.1;
    k2   = 2;
    m    = 1;
    x_i1 = -0.5;
    x_i2 = 0;
    
    t = 1:0.1:20;
    Y = analytic_osc_n_eq_2(t, x_i1, x_i2, k1, k2, m, false, false);
    x1 = Y(1, :);
    x2 = Y(2, :);
    v1 = diff(x1); v1(end+1) = v1(end);
    v2 = diff(x2); v2(end+1) = v2(end);
    
    %% 
    spring = Spring(0.3, 10);
    w1 = [0 1];
    w2 = [3 1];
    sl_third = norm(w2-w1) /3; % sl(SystemLength)
    eqp1 = w1 + [sl_third, 0];
    eqp2 = w1 + [sl_third * 2, 0];
    
    %%
    subp = @(a) subplot(3, 2, a);
    animplot = @() subp([5 6]);
    
    function subp_limiter(i, x, y)
        subp(i);
        xlim([min(x), max(x)]);
        ylim([min(y), max(y)]);
        hold on;
    end
    subp_limiter(1, t, x1);
    subp_limiter(2, t, x2);
    subp_limiter(3, t, v1);
    subp_limiter(4, t, v2);
    
    animplot();
    xlim([0, 3]);
    ylim([0, 2]);
    hold on;
    
    function subp_ploter(subp_i, i, x, y, title_str)
        subp(subp_i);
        cla;
        plot(x(1:i), y(1:i));
        yline(0);
        title(title_str, 'Interpreter', 'latex');
    end
    
    j = 1;
    for i = 1:length(t)
        cla;
        
        subp_ploter(1, i, t(1:i), x1(1:i), "1. mass displ.");
        subp_ploter(2, i, t(1:i), x2(1:i), "2. mass displ.");
        
        subp_ploter(3, i, t(1:i), v1(1:i), "1. mass velocity");
        subp_ploter(4, i, t(1:i), v2(1:i), "2. mass velocity");
        
        animplot();
        draw(i, t, x1, x2, w1, w2, eqp1, eqp2, spring);
        title("animation");
        
%         pause(0.01);
        drawnow;
        j = i;
    end
end


function draw(i, t, x1, x2, w1, w2, eqp1, eqp2, spring)
    title(t(i));

    xline(w1(1));
    xline(eqp1(1));
    xline(eqp2(1));
    xline(w2(1));

    m1pos = eqp1 + [x1(i), 0];
    m2pos = eqp2 + [x2(i), 0];

    [springX, springY] = spring.getSpr(w1, m1pos);
    plot(springX,springY, 'color', 'blue');
    
    [springX, springY] = spring.getSpr(m1pos, m2pos);
    plot(springX,springY, 'color', 'blue');
    
    [springX, springY] = spring.getSpr(m2pos, w2);
    plot(springX,springY, 'color', 'blue');
    
    pointMass(m1pos);
    pointMass(m2pos);
end

function pointMass(C)
    rectangle('Position',[C(1)-0.05, C(2)-0.05, .1, .1],'FaceColor',[0 0 0]);
end