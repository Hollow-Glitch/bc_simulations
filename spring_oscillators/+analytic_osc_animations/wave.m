
function wave()
% wave - Animation of the: n -> infty case / Wave Equation (no damping)

    %% system constants
    tspan = [0 100];
    time_granularity = 0.1;
    space_granularity = 0.1;
    
    l = 10;
    n = 50;
    
    m = 1;
    k = 1;
    
    delta_x = l / n;
    
    rho = m / delta_x;
    E = k * delta_x;
    
    c = sqrt(E / rho);
    
    %% convenience functions
    f = @(x) f_l(x, l);
    
    subp = @(x) subplot(2,1,x);
    
    %% plot of the initial displacement function
    subp(1);
        x = 0:0.1:l;
        f_of_x = f(x);
        plot(x, f_of_x);
        xline(0);
        yline(0);
        f_min = min(f_of_x);
        f_max = max(f_of_x);
        f_dif = f_max - f_min;
        ylim([f_min - 0.2*f_dif, f_max + 0.2*f_dif]);
        title('Initial displacement function $f(x)$', 'Interpreter', 'latex');
        xlabel('$x$', 'Interpreter', 'latex');
        ylabel('$f(x)$', 'Interpreter', 'latex');

    %% preparing to plot displacement in time
    % prepapre for 
    c_n = zeros(n, 1);
    for n = 1:n
        c_n(n) = integral(@(x) f(x) .* sin(n*pi*x/l), 0, l, 'ArrayValued', true);
    end
    
    time_range = tspan(1):time_granularity:tspan(2);
    space_range = 0:space_granularity:l;
    u_tx = zeros(length(time_range), length(space_range));
%         for t = tspan(1):time_granularity:tspan(2)
%             for x = 1:space_granularity:l
%                 u(t, x, c_n, c);
%             end
%         end
    for t = 1:length(time_range)
        for x = 1:length(space_range)
            u_tx(t, x) = u(time_range(t), space_range(x), c_n, c, l);
        end
    end
    
    %% animation plot of displacement in time
    subp(2);
        u_tx_min = min(u_tx, [], 'all');
        u_tx_max = max(u_tx, [], 'all');
        u_tx_dif = u_tx_max - u_tx_min;
        ylim([u_tx_min - 0.2*u_tx_dif, u_tx_max + 0.2*u_tx_dif]);
        xlim([0, l]);
        xlabel('$x$', 'Interpreter', 'latex');
        ylabel('$u(t,x)$', 'Interpreter', 'latex');
        title('Evolution of $u(t,x)$ over time', 'Interpreter', 'latex');
        hold on;
        for t = 1:length(time_range)
            cla;
            xline(0);
            yline(0);
            plot(space_range, u_tx(t, :) );
            pause(0.01);
        end
end

%% initial displacement function definition
function Y = f_l(x, l)
    function y = f(x)
        if x < 0
            % (-infty, 0)
            y = 0;
        elseif x < (l/4)
            % (0, l/4)
            y = x;
        elseif x < (l/2)
            % (l/4, l/2)
            y = 2*(l/4)-x;
        else
            % (l/2, infty)
            y = 0;
        end
    end

    Y = zeros(length(x), 1);
    for i = 1:length(x)
        Y(i) = f(x(i));
    end
    
    % return Y
end

%% displacement of x-th at t
function y = u(t,x, c_n, c, l)
    n = length(c_n);
    y = 0;
    for n = 1:n
        y = y + c_n(n) * sin(n*pi*x/l) * cos(c*n*pi*t/l);
    end
end

