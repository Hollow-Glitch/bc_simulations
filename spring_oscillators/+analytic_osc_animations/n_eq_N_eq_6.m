

function n_eq_N_eq_6()
% n_eq_N_eq_6 - Animation of the: 6 Coupled Oscillators case (no damping)

    n=6; 
    k=1; 
    m=1;
    x_Ij = zeros(n, 1);
    v_Ij = zeros(n, 1);
    
    x_Ij(1) = -0.9;
    %v_Ij(1) = 0.5;
    

    t = 0:0.1:15;
    Y = analytic_osc_n_eq_N(t, k, m, x_Ij)';
    x_j = Y(:, 1:n);
    v_j = Y(:, (n+1):(2*n) );
    
    spring = Spring(0.3, 3);
    subp = @(a) subplot(5, 3, a);
    animplot = @() subp([7 8 9]);
    minT = min(t);
    maxT = max(t);
    for j = 1:n
        subp(j);
        xlim([minT, maxT]);
        ylim([min(x_j(:, j)), max(x_j(:,j))]);
        plot(t, x_j(:,j));
        yline(0);
        title("x_"+j);
        hold on;
    end
    for j = 1:n
        subp(9+j);
        xlim([minT, maxT]);
        ylim([min(v_j(:, j)), max(v_j(:,j))]);
        plot(t, v_j(:,j));
        yline(0);
        title("v_"+j);
        hold on;
    end
    
    w1 = [0, 1];
    w2 = [n+1, 1];
    osc_spacing = norm(w2-w1) / (n+1);
    eq_j = zeros(n, 2);
    for i = 1:n
        eq_j(i, :) = w1 + [osc_spacing, 0] * i;
    end
    
    pointMass = @(C) rectangle('Position',[C(1)-0.05, C(2)-0.05, .1, .1],'FaceColor',[0 0 0]);
    m_j_pos = zeros(n, 2);
    for i = 1:length(t)
        animplot();
        cla;
        hold on;
        
        xline(w1(1));
        for j = 1:n
            xline(eq_j(j, 1));
            %disp("eq_j(" + j + ",:) = [" + eq_j(j,1) + ", " + eq_j(j,2) + "]");
            m_j_pos(j,:) = eq_j(j,:) + [x_j(i, j), 0];
            %disp(m_j_pos(j,:) + " = " + eq_j(j,:) + " + " + [x_j(i), 0]);
        end
        xline(w2(1));
        
        [springX, springY] = spring.getSpr(w1, m_j_pos(1,:));
        plot(springX, springY, 'color', 'blue');
        for j = 1:(n-1)
            pointMass(m_j_pos(j,:));
            [springX, springY] = spring.getSpr(m_j_pos(j,:), m_j_pos(j+1,:));
            plot(springX, springY, 'color', 'blue');
        end
        pointMass(m_j_pos(n,:));
        [springX, springY] = spring.getSpr(m_j_pos(n,:), w2);
        plot(springX, springY, 'color', 'blue');
        
        drawnow;
%         pause(0.01);
    end
end