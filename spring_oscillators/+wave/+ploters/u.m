

function surf_type = u(time_range, space_range, c_1n, c, l)
% c_1n  ...  array of constants

    [t, x] = meshgrid(time_range, space_range);
    h = surf(t, x, ...
        arrayfun(@(t, x) wave.u(t, x, c_1n, c, l), t, x) ...
    );
    set(h,'LineStyle','none');
    xlabel('t');
    ylabel('x');
    zlabel('u(t,x)');
    
    surf_type = h;
end