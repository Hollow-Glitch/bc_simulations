

function line_type = u_n_at_0x(space_range, c_1n, n, c, l)  % t = 0, u_n_tx -> u_n_0x
%  c_1n  ...  scalar constant
%  n     ...  index

    u_1_0x = zeros(length(space_range), 1);
    for i = 1:length(space_range)
        x = space_range(i);
        u_1_0x(i) = wave.u_n(0, x, c_1n, n, c, l);
    end
    h = plot(space_range, u_1_0x);
    ylabel(sprintf("u_%d(0, x)", n));
    xlabel('x');
    xline(0);
    yline(0);

    line_type = h;
end