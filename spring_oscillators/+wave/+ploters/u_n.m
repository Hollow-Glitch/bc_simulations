

function surf_type = u_n(time_range, space_range, c_1n, n, c, l)
%  time_range   ...  like: 0:0.1:100
%  space_range  ...  like: 0:0.1:100
%  c_1n         ...  scalar constant
%  n            ...  index
%  l            ...  system length

    [t, x] = meshgrid(time_range, space_range);
    h = surf(t, x, ...
        arrayfun(@(t, x) wave.u_n(t, x, c_1n, n, c, l), t, x) ...
    );
    set(h,'LineStyle','none');
    xlabel('t');
    ylabel('x');
    zlabel(sprintf('u_%d(t,x)', n) );
    
    surf_type = h;
end
