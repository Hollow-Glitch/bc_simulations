

function out = u_n(t, x, c_1n, n, c, l)
% t    ...  time
% x    ...  position
% c_n  ...  scalar, constant, calculate before, with: wave.c_1n(...)
% n    ...  index
% l    ...  system length
% c    ...  constant

    out = c_1n * sin(n*pi*x/l) * cos(c*n*pi*t/l);
end
