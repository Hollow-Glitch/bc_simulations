

function out = c_1n(f, l, n)
%  f  ...  f(x)
%  l  ...  system length
%  n  ...  index

    out = integral(@(x) f(x) .* sin(n*pi*x/l), 0, l, 'ArrayValued', true);
end

