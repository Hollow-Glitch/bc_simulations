

function out = u(t, x, c_1n, c, l)
% t    ...  time
% x    ...  position
% c_1n ...  array of constants; calculate before, with: wave.c_1n(...)
% N    ...  even though n->oo, numerically, n = 1, 2, ..., N
% l    ...  system length
% c    ...  constant

    N = length(c_1n);
    y = 0;
    for n = 1:N
        y = y + wave.u_n(t, x, c_1n(n), n, c, l);
    end
    out = y;
end
