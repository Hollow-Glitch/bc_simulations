

function y = spike(x, l)
    if x < 0
        % (-infty, 0)
        y = 0;
    elseif x < (l/4)
        % (0, l/4)
        y = x;
    elseif x < (l/2)
        % (l/4, l/2)
        y = 2*(l/4)-x;
    else
        % (l/2, infty)
        y = 0;
    end
end
