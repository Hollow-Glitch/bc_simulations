


t = 0 : 0.1 : 20;
t2 = 0 : 0.1 : 100;
t3 = 0 : 0.1 : 60;
% <<
sugar(t, 1, 0, 0, 0, 0.5,1,1, "<<");
% =<
sugar(t, 1, 0, 0, 0, 0.25,1,1, "=<");
% ><
sugar(t, 1, 0, 0, 0, 0.2,1,1, "><");
% >=
sugar(t3, 1, 0, 0, 0, 0.08333,1,1, ">=");
% >>
sugar(t2, 1, 0, 0, 0, 0.05,1,1, ">>");



function sugar(t, x_I1, x_I2, v_I1, v_I2, k, m, b, name)
    x = analytic_osc_n_eq_2_damped(t, x_I1, x_I2, v_I1, v_I2, k, m, b);
    plot(t, x);
    hold on;
    yline(0);
    name = sprintf('osc_n=2_damped_analytic__%s__x1=%f_x2=%f_v1=%f_v2=%f_k=%f_m=%d_b=%d', name, x_I1, x_I2, v_I1, v_I2, k, m, b);
    smart_plot_print(name);
    hold off;
end