

function output = n_eq_N(varargin)
%ANALYTIC_OSC_N_EQ_N_EVERY_CASE
% Optional arguments
% - 'return_api'        ... type: scalar & logical, default: false
%                           returns a struct(let it's name be 'api'), 
%                           api.<Tab>
%                           results in the IDE giving you a list to pick
%                           the field from
% - 'gen_eigen_plots'   ... type: scalar & logical, default: false
% - 'gen_N_eq_2_plots'     ... type: scalar & logical, default: false
% - 'gen_N_eq_3_plots'     ... type: scalar & logical, default: false
% - 'gen_N_eq_6_plots'     ... type: scalar & logical, default: false
% - 'gen_analytic_numeric_comparison'   ...    type: scalar & numeric, default: 0


    p = inputParser();
    p.addOptional('return_api',       false, @(x) islogical(x) && isscalar(x) );
    p.addOptional('gen_eigen_plots',  false, @(x) islogical(x) && isscalar(x) );
    p.addOptional('gen_N_eq_2_plots', false, @(x) islogical(x) && isscalar(x) );
    p.addOptional('gen_N_eq_3_plots', false, @(x) islogical(x) && isscalar(x) );
    p.addOptional('gen_N_eq_6_plots', false, @(x) islogical(x) && isscalar(x) );
    p.addOptional('gen_analytic_numeric_comparison', 0, @(x) isnumeric(x) && isscalar(x) );
    p.parse(varargin{:});
    args = p.Results;

    api = cell2struct(p.Parameters, p.Parameters, 2); % the third parameter for returning the api always must be '2'
    
    if args.return_api
        output = api;
        return;
    end
    
    function if_close_do(bool_or_int, func)
        if bool_or_int
            close all;
            func();
        end
    end

    if_close_do(args.gen_eigen_plots,  @() n_eq_N_eigen_print_plot(50, 3, 1, 1, 2)      );
    if_close_do(args.gen_eigen_plots,  @() n_eq_N_eigen_print_plot(50, 3, 1, 50-1, 50)  );
    
    if_close_do(args.gen_N_eq_2_plots, @() generate_plots_n_eq_N_eq_2()                 );
    if_close_do(args.gen_N_eq_3_plots, @() generate_plots_n_eq_N_eq_3()                 );
    if_close_do(args.gen_N_eq_6_plots, @() generate_plots_n_eq_N(0:0.1:10, 6)           );
    
    N = args.gen_analytic_numeric_comparison;
    if_close_do(N, @() gen_ana_num_comp(N) );
    
    output = api;
end


%% generate analytic numeric comparison

function gen_ana_num_comp(N)
%     N = 3;
    
    x_Ij = zeros(N, 1);
    v_Ij = zeros(N, 1);
    x_Ij(1) = 1;
    IC = [ x_Ij(:) v_Ij(:) ] .';
    IC = IC(:);
    
    [t,Y] = ode45(@(t,y) model_osc_n_eq_N(t,y,N,1,1), [0,10], IC);
    t_num = t;
    x_num = Y(:, 1:2:2*N);
%     v_j = Y(:, 2:2:2*N);

    t_ana = 0:0.1:10;
    x_ana = analytic_osc_n_eq_N(t_ana, 1, 1, x_Ij)';

    subp = @(a) subplot(3, 1, a);
%     colors_num = spring(N);
%     colors_ana = winter(N);
    
    function plot_num(N, t_num, x_num, style)
        colors_num = lines(N);
        hold on;
        for i = 1:N
            plot(t_num, x_num(:, i), style, 'Color', colors_num(i,:) );
        end
        yline(0);
        ylabel('numerical');
    end

    function plot_ana(N, t_ana, x_ana, style)    
        colors_ana = hsv(N);
        hold on;
        for i = 1:N
            plot(t_ana, x_ana(:, i), style, 'Color', colors_ana(i,:) );
        end
        yline(0);
        ylabel('analytical');
    end
    
    subp(1);    
%     hold on;
%     for i = 1:N
%         plot(t_num, x_num(:, i), 'Color', colors_num(i,:) );
%     end
%     yline(0);
    plot_num(N, t_num, x_num, '-');
    
    subp(2);    
%     hold on;
%     plot(t_ana, x_ana)
%     yline(0);
    plot_ana(N, t_ana, x_ana, '-');
    
    subp(3);
    hold on;
%     hold on;
%     plot(t_num, x_num);
%     plot(t_ana, x_ana, '--');
%     yline(0);
    plot_num(N, t_num, x_num, '-');
    plot_ana(N, t_ana, x_ana, '--');
    ylabel('comparison');

    name = sprintf('osc_n=N_comparison_ana_num__N=%d', N);
    smart_plot_print(name);
end


% generators for cases
% - N=2
% - N=3
% - N=6

function generate_plots_n_eq_N_eq_2()
    t = 0:0.1:10;
    k = 1;
    m = 1;
    IC_1 = [-1; 1];
    IC_2 = [ 1; 1];
    
    n_eq_N_print(t, k, m, IC_1, "1", 2);
    n_eq_N_print(t, k, m, IC_2, "2", 2);
end


function generate_plots_n_eq_N_eq_3()
    t = 0:0.1:10;
    k = 1;
    m = 1;
    IC_1 = [  1   ;  2    ; 1 ];
    IC_2 = [  1   ;  1.41 ; 1 ];
    IC_3 = [  1   ; -1.41 ; 1 ];
    IC_4 = [ -2.4 ;  1    ; 1 ];
    IC_5 = [  0.41;  1    ; 1 ];
    IC_6 = [ -1   ;  1    ; 1 ];
    
    n_eq_N_print(t, k, m, IC_1, "1", 3);
    n_eq_N_print(t, k, m, IC_2, "2", 3);
    n_eq_N_print(t, k, m, IC_3, "3", 3);
    n_eq_N_print(t, k, m, IC_4, "4", 3);
    n_eq_N_print(t, k, m, IC_5, "5", 3);
    n_eq_N_print(t, k, m, IC_6, "6", 3);
end


% function generate_plot_n_eq_N_based_on_c_vec(c)
%         c = c(:);
%         N = numel(c);
%         M = analytic_osc_n_eq_N_construct_M(N);
%         x_I = M * c;
%         
%         ID = strjoin(string(c), '_');
%         ID = strjoin(['c=[', ID , ']'], '');
%         
%         n_eq_N_print(t, k, m, x_I, ID, 3);
% end


function generate_plots_n_eq_N(t, N)
    k=1;
    m=1;
    
    M = analytic_osc_n_eq_N_construct_M(N);
    
    nonmc = @(n) 2^n-1; % number of normal mode combinations
    gen = @(n) de2bi(1:nonmc(n))';
    c_1i = gen(N);
    
    for i = 1:nonmc(N)
        c = c_1i(:,i);
        x_I = M * c;
        ID = strjoin(string(c), '_');
        ID = strjoin(['c=[', ID , ']'], '');
        n_eq_N_print(t, k, m, x_I, ID, 3);
    end
end


%% print functions


function n_eq_N_print(t, k, m, IC, ID_str, legendColCount)
    x = analytic_osc_n_eq_N(t, k, m, IC);
    
    N = numel(IC(:));
    labels_strList = strings(N, 1);
    for i = 1:N
        labels_strList(i) = sprintf('x_%d(t)', i);
    end
    
    IC_str = strjoin(string(IC), "_");
    
    plot(t, x);
    legend(labels_strList, 'Location', 'northoutside', 'NumColumns', legendColCount, 'AutoUpdate', 'off');
    hold on;
    yline(0);
    name = sprintf('osc_n=N_analytic__N=%d_ID=%s_k=%f_m=%f_IC=[%s]', N, ID_str, k, m, IC_str );
    smart_plot_print(name);
    hold off;
end


function n_eq_N_eigen_print_plot(N, k, m, e1, e2)
    analytic_osc_n_eq_N_eigen(N, k, m, e1, e2);
    name = sprintf('osc_n=N_analytic__N=%d_k=%f_m=%f_e1=%d_e2=%d', N, k, m, e1, e2);
    smart_plot_print(name);
end