
close all;

k = 1;
m = 1;

f = @(x_I1, x_I2, x_I3) analytic_osc_n_eq_3(0:0.01:10, x_I1, x_I2, x_I3, k, m, true, true);

f(1, 2/sqrt(2), 1); close all; % c_1 = 0 = c_3
f(-1, 0, 1); close all; % c_3 = 0 = c_5
f(1, -2/sqrt(2), 1); close all; % c_1 = 0 = c_5

f(1, 2, 1); close all; % c_1 = 0
f(sqrt(2)-1, 1, 1); close all; % c_3 = 0
f(-sqrt(2)-1, 1, 1); close all; % c_5 = 0

