
m = 1;
create_plot = true;
print_plot = false;
name_sys_const = false;


% antisymmetric
close all;
analytic_osc_n_eq_2(0:0.01:10, 1, -1, 1, 1, m, true, true);

% symmetric
close all;
analytic_osc_n_eq_2(0:0.01:10, 1,  1, 1, 1, m, true, true);

% beats: k_1=4, k_2=0.5
close all;
analytic_osc_n_eq_2(0:0.01:40, 2, 0, 4, 0.5, m, true, true);

close all;
analytic_osc_n_eq_2_beats(0:0.01:40, 4, 0.5, 1, true, true);
% 
% 
% t = 0:0.01:40;
% x1 = analytic_osc_n_eq_2(t, 2, 0, 2, 1, m, false, false);
% x2 = analytic_osc_n_eq_2(t, 2, 0, 3, 0.75, m, false, false);
% x3 = analytic_osc_n_eq_2(t, 2, 0, 4, 0.5, m, false, false);
% close all;
% ribbon(t, x1(1,:));
% ribbon(t, x2(1,:));
% ribbon(t, x3(1,:));