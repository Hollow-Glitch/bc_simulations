
function wave()
    %% system constants
    tspan = [0 100];
    time_granularity = 0.1;
    
    l = 10;
    N = 50;
    delta_x = l / N;
    
    time_range = tspan(1):time_granularity:tspan(2);
    space_range = 0:delta_x:l;
    
    m = 1;
    k = 1;
    
    rho = m / delta_x;
    E = k * delta_x;
    
    c = sqrt(E / rho);
    
    %% f(x)
    f = @(x) wave.init_displ_funcs.spike(x, l);
    
    %% c_1n, n=1:N
    c_1n = zeros(N, 1);
    for n = 1:N
        c_1n(n) = wave.c_1n(f, l, n);
    end
    
    %% plot f(x)
    x = 0:0.1:l;
    f_of_x = arrayfun(f, x);
    h = plot(x, f_of_x);
        xline(0);
        yline(0);
        f_min = min(f_of_x);
        f_max = max(f_of_x);
        f_dif = f_max - f_min;
        ylim([f_min - 0.2*f_dif, f_max + 0.2*f_dif]);
        xlabel('x');
        ylabel('u(0, x)');
        
    utils_v2.figure_decorators.bc_style_squareish(h, 'width_multiplier', 2);
    name = sprintf('osc_wave_ID=IC_func');
    fprintf("saving figure to file with name: %s\n", name);
    utils_v2.smart_plot_print(h, name);
    
    %% u_n surf, for several n
    function u_n_surf(n)
        h = wave.ploters.u_n(time_range, space_range, c_1n(n), n, c, l);
        utils_v2.figure_decorators.bc_style_squareish(h, 'width_multiplier', 1);
        name = sprintf('osc_wave_ID=%d', n);
        fprintf("saving figure to file with name: %s\n", name);
        utils_v2.smart_plot_print(h, name);
    end
    
    close all;
    u_n_surf(1);
    u_n_surf(2);
    u_n_surf(3);
    u_n_surf(4);
    u_n_surf(5);
    
    %% u surf
    close all;
    h = wave.ploters.u(time_range, space_range, c_1n, c, l);
    utils_v2.figure_decorators.bc_style_squareish(h, 'width_multiplier', 2, 'height_multiplier', 2);
    name = sprintf('osc_wave_ID=all');
    fprintf("saving figure to file with name: %s\n", name);
    utils_v2.smart_plot_print(h, name);
    
    %% u_n(0,x)
    function u_n_t_eq_0_plot(n)
        h = wave.ploters.u_n_at_0x(space_range, c_1n(n), n, c, l);
        utils_v2.figure_decorators.bc_style_squareish(h, 'width_multiplier', 1);
        name = sprintf('osc_wave_u_%d(0, x)', n);
        fprintf("saving figure to file with name: %s\n", name);
        utils_v2.smart_plot_print(h, name);
    end
    
    close all;
    u_n_t_eq_0_plot(1);
    u_n_t_eq_0_plot(2);
    u_n_t_eq_0_plot(3);
    u_n_t_eq_0_plot(4);
    u_n_t_eq_0_plot(5);

    
    %% u(t,x), for several t
    u_tx = zeros(length(time_range), length(space_range));
    for t = 1:length(time_range)
        for x = 1:length(space_range)
            u_tx(t, x) = wave.u(time_range(t), space_range(x), c_1n, c, l);
        end
    end
    u_tx_min = min(u_tx, [], 'all');
    u_tx_max = max(u_tx, [], 'all');
    u_tx_dif = u_tx_max - u_tx_min;
    y_min_max = [u_tx_min - 0.2*u_tx_dif, u_tx_max + 0.2*u_tx_dif];
    
    function u_tx_f(t_i)
        % t_i  ...  time_range index
        h = plot(space_range, u_tx(t_i,:) );
        ylim(y_min_max);
        xlim([0, l]);
        xline(0);
        yline(0);
        xlabel('x');
        ylabel(sprintf('u(%d,x)', time_range(t_i)));

        utils_v2.figure_decorators.bc_style_squareish(h, 'width_multiplier', 1);
        name = sprintf('osc_wave_u(%d, x)', time_range(t_i) );
        fprintf("saving figure to file with name: %s\n", name);
        utils_v2.smart_plot_print(h, name);
    end

    wanted_values_of_t = 1:3:3*6+1;
    time_range_indexes = arrayfun(@(x) find(time_range==x), wanted_values_of_t);
    arrayfun(@(time_range_index) u_tx_f(time_range_index), time_range_indexes);

    close all;
end

