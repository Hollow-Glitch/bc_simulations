

close all;
missing_c1_x_I1(0:0.01:10, 1, 1, 1, true, false);

close all;
missing_c1_x_I2(0:0.01:10, 1, 1, 1, true, false);

close all;
missing_c3_x_I1(0:0.01:10, 1, 1, 1, true, false);

close all;
missing_c3_x_I2(0:0.01:10, 1, 1, 1, true, false);

%analytic_osc_n_eq_2_golden(0:0.01:500, 2, -0.21, 0.001, 1, true, false);



%% missing c_1
function missing_c1_x_I1(t, x_I1, k, m, create_plot, print_plot)
    p = (sqrt(5) + 1) /2;
    x_I2 = x_I1 * p;
    analytic_osc_n_eq_2_golden(t, x_I1, x_I2, k, m, create_plot, print_plot);
    name = sprintf('osc_n=2_golden_analytic__c_1=0_set_by_x_I1__x_I1=%.2f_x_I2=%.2f_k=%.2f_m=%.2f', ...
                    x_I1, x_I2, k, m);
    smart_plot_print(name);
end

function missing_c1_x_I2(t, x_I2, k, m, create_plot, print_plot)
    p = (sqrt(5) + 1) /2;
    x_I1 = x_I2 / p;
    analytic_osc_n_eq_2_golden(t, x_I1, x_I2, k, m, create_plot, print_plot);
    name = sprintf('osc_n=2_golden_analytic__c_1=0_set_by_x_I2__x_I1=%.2f_x_I2=%.2f_k=%.2f_m=%.2f', ...
                    x_I1, x_I2, k, m);
    smart_plot_print(name);
end

%% missing c_3
function missing_c3_x_I1(t, x_I1, k, m, create_plot, print_plot)
    p = (sqrt(5) + 1) /2;
    x_I2 = (1-p)*x_I1;
    analytic_osc_n_eq_2_golden(t, x_I1, x_I2, k, m, create_plot, print_plot);
    name = sprintf('osc_n=2_golden_analytic__c_2=0_set_by_x_I1__x_I1=%.2f_x_I2=%.2f_k=%.2f_m=%.2f', ...
                    x_I1, x_I2, k, m);
    smart_plot_print(name);
end

function missing_c3_x_I2(t, x_I2, k, m, create_plot, print_plot)
    p = (sqrt(5) + 1) /2;
    x_I1 = x_I2 / (1 - p);
    analytic_osc_n_eq_2_golden(t, x_I1, x_I2, k, m, create_plot, print_plot);
    name = sprintf('osc_n=2_golden_analytic__c_2=0_set_by_x_I2__x_I1=%.2f_x_I2=%.2f_k=%.2f_m=%.2f', ...
                   x_I1, x_I2, k, m);
    smart_plot_print(name);
end