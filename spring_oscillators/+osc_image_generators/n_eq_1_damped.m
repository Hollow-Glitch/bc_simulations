
t = [0:0.1:20];

% varying c
analytic_osc_n_eq_1_damped(t, 1, 0, 1, 1, 0.5, true, true, false); % under
analytic_osc_n_eq_1_damped(t, 1, 0, 1, 1, 2, true, true, false); % crit
analytic_osc_n_eq_1_damped(t, 1, 0, 1, 1, 3, true, true, false); % over
close all;

% % varying k
% analytic_osc_n_eq_1_damped(t, 1, 0, 1, 0.3, 1, true, true);
% analytic_osc_n_eq_1_damped(t, 1, 0, 1, 0.25,  1, true, true);
% analytic_osc_n_eq_1_damped(t, 1, 0, 1, 0.2, 1, true, true);
% close all;

hold on;
analytic_osc_n_eq_1_damped(t, 1, 0, 1, 1, 0.5, true, false, false);
analytic_osc_n_eq_1_damped(t, 1, 0, 1, 1, 2, true, false, false);
analytic_osc_n_eq_1_damped(t, 1, 0, 1, 1, 3, true, false, false);
L(1) = plot(nan, nan, 'b-');
L(2) = plot(nan, nan, 'y-');
L(3) = plot(nan, nan, 'r-');
legend(L, {'under damped', 'over damped', 'critically damped'})
smart_plot_print('osc_n=1_analytic_every_case');
close all;

% analytic_osc_n_eq_1_damped(t, 1, 0, 1, 0.3, 1, true, false);
% analytic_osc_n_eq_1_damped(t, 1, 0, 1, 0.25,  1, true, false);
% analytic_osc_n_eq_1_damped(t, 1, 0, 1, 0.2, 1, true, false);
% smart_plot_print('osc_n=1_analytic_every_case_varying_k');
% close all;