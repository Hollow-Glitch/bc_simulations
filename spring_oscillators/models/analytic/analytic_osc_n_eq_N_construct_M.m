

function M = analytic_osc_n_eq_N_construct_M(N)
    phi = @(n, N) (n .* pi) ./ (N + 1);
    
    % construct M
    Msin = @(rowIndex, colIndex, N) sin(rowIndex*phi(colIndex, N));
    M = zeros(N ,N);
    for rowIndex = 1:N
        for colIndex = 1:N
            M(rowIndex, colIndex) = Msin(rowIndex, colIndex, N);
        end
    end
end
