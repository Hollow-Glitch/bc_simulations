function x = analytic_osc_n_eq_2_beats(t, k_1, k_2, m, create_plot, print_plot)
% This function assumes that x_I1 = 2, x_I2 = 0
    %% safe checks
%     if (x_I1 == 0) & (x_I2 == 0)
%         disp("Without initial displacement the system doesn't perform any motion");
%         return;
%     end
    if ~(k_1 > 0) | ~(k_2 > 0)
        disp("Error not sattisfied: k>0");
        return;
    end
    if ~(m > 0)
        disp("Error not sattisfied: m>0");
        return;
    end
    
    %% computations

    % f_i - omega_i, i=1,2
    f_1 = sqrt(k_1 / m);
    f_2 = sqrt((k_1 + 2*k_2) / m);

%     c_1 = (x_I1 + x_I2) / 2;
%     c_3 = (x_I1 - x_I2) / 2;

    
    f = (f_1 + f_2) / 2;
    e = (f_1 - f_2) / 2;
    A_1 = @(t) 2*sin(e.*t);
    A_2 = @(t) 2*cos(e.*t);
    x_1 = @(t) A_1(t).*sin(f.*t);
    x_2 = @(t) A_2(t).*cos(f.*t);
    
    %% additional
    if create_plot
        if print_plot
            ploting
        end
        
        hold on;
%         plot(t, x(1,:), 'b-');
%         plot(t, x(2,:), 'r--');
        p_x_1 = plot(t, x_1(t), '-');
        p_x_2 = plot(t, x_2(t), '-');
        p_A_1 = plot(t, A_1(t), '-');
        p_A_2 = plot(t, A_2(t), '-');
        ylim([min(min([x_1(t), x_2(t)]))*1.25, max(max([x_1(t), x_2(t)]))*1.25]);
        legend([p_x_1,p_A_1, p_x_2, p_A_2],'x_1(t)', 'A_1(t)', 'x_2(t)', 'A_2(t)', 'AutoUpdate', 'off');
        xlabel('t');
        ylabel('x(t), A(t)');
        xline(0);
        yline(0);
        
        if print_plot
%             if name_sys_const
                name = sprintf('osc_n=2_analytic_beats__k_1=%.2f_k_2=%.2f_m=%.2f', ...
                    k_1, k_2, m);
%             else
%                 name = sprintf('osc_n=1_analytic_%s__x_I=%.2f_v_I=%.2f_f=%.2f_xi=%.2f_m=%.2f', ...
%                     what_damped, x_0, v_0, f, xi, m);
%             end
            smart_plot_print(name);
        end
    end
end