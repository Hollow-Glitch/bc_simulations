

function X = analytic_osc_n_eq_2_damped(t, x_I1, x_I2, v_I1, v_I2, k, m, b)    
    %% safe checks
    if (x_I1 == 0) && (x_I2 == 0) && (v_I1 == 0) && (v_I2 == 0)
        disp("Without initial displacement or initial velocity the system doesn't perform any motion");
        return;
    end
    if ~(k > 0)
        disp("Error not sattisfied: k>0");
        return;
    end
    if ~(m > 0)
        disp("Error not sattisfied: m>0");
        return;
    end

    %% computations
    
    % sys. compound constants
    o_1 = sqrt(k/m);
    o_2 = sqrt((3*k)/m);
    xi_1 = b/(2*sqrt(m*k));
    xi_2 = b/(2*3*sqrt(m*k));
    l_1p = o_1*(-xi_1 + sqrt(xi_1^2 -1));
    l_1m = o_1*(-xi_1 - sqrt(xi_1^2 -1));
    l_2p = o_2*(-xi_2 + sqrt(xi_2^2 -1));
    l_2m = o_2*(-xi_2 - sqrt(xi_2^2 -1));

    
    % ------------------------------------------------------------
    u_1 = @(t, c_11, c_12) (...
        c_11 .*exp(-o_1.*xi_1.*t) .*cos(o_1.*sqrt(1-xi_1^2).*t) +...
        c_12 .*exp(-o_1.*xi_1.*t) .*sin(o_1.*sqrt(1-xi_1^2).*t)...
    );
%     u_2 = @(t, c_11, c_12) (...
%         c_11.*exp(o_1.*(-xi_1+sqrt(xi_1^2-1)).*t) +...
%         c_12.*exp(o_1.*(-xi_1-sqrt(xi_1^2-1)).*t)...
%     );
    u_2 = @(t, c_11, c_12) (...
        c_11.*exp(l_1p.*t) +...
        c_12.*exp(l_1m.*t)...
    );
    u_3 = @(t, c_11, c_12) (...
        c_11.*exp(-o_1.*xi_1.*t) +...
        c_12.*exp(-o_1.*xi_1.*t)...
    );
    % ============================================================
    v_1 = @(t, c_21, c_22) (...
        c_21 .*exp(-o_2.*xi_2.*t) .*cos(o_2.*sqrt(1-xi_2^2).*t) +...
        c_22 .*exp(-o_2.*xi_2.*t) .*sin(o_2.*sqrt(1-xi_2^2).*t)...
    );
%     v_2 = @(t, c_21, c_22) (...
%         c_21.*exp(o_2.*(-xi_2+sqrt(xi_2^2-1)).*t) +...
%         c_22.*exp(o_2.*(-xi_2-sqrt(xi_2^2-1)).*t)...
%     );
    v_2 = @(t, c_21, c_22) (...
        c_21.*exp(l_2p.*t) +...
        c_22.*exp(l_2m.*t)...
    );
    v_3 = @(t, c_21, c_22) (...
        c_21.*exp(-o_2.*xi_2.*t) +...
        c_22.*exp(-o_2.*xi_2.*t)...
    );
    % ------------------------------------------------------------

    A = [1, -1;
         1,  1];
    
    if     xi_1  < 1 && xi_2  < 1
        B =   [        1, 0;
                o_1*xi_1, 1];
        c_1 = B * [x_I1 + x_I2; v_I1 + v_I2];
        c_2 = B * [x_I1 - x_I2; v_I1 - v_I2];
        x = @(t) (1/2).*A * [ 
            u_1(t, c_1(1), c_1(2)) ; 
            v_1(t, c_2(1), c_2(2)) 
        ];
    elseif xi_1 == 1 && xi_2  < 1
        c_11 = x_I1 + x_I2;
        c_12 = v_I1 + v_I2 + (x_I1 + x_I2)*o_1*xi_1;
        B =   [        1, 0;
                o_1*xi_1, 1];
        c_2 = B * [x_I1 - x_I2; v_I1 - v_I2];
        x = @(t) (1/2).*A * [ 
            u_3(t, c_11, c_12) ; 
            v_1(t, c_2(1), c_2(2)) 
        ];
    elseif xi_1  > 1 && xi_2  < 1
        C = (1/(l_1m - l_1p)) * [
                 l_1m, -1;
                -l_1p,  1
            ];
        B =   [        1, 0;
                o_1*xi_1, 1];
        c_1 = C * [x_I1 + x_I2; v_I1 + v_I2];
        c_2 = B * [x_I1 - x_I2; v_I1 - v_I2];
        x = @(t) (1/2).*A * [ 
            u_2(t, c_1(1), c_1(2)) ; 
            v_1(t, c_2(1), c_2(2)) 
        ];
    elseif xi_1  > 1 && xi_2 == 1        
        B =   [        1, 0;
                o_1*xi_1, 1];
        c_1 = B * [x_I1 + x_I2; v_I1 + v_I2];
        c_21 = x_I2 - x_I1;
        c_22 = v_I2 - v_I1 + (x_I2 - x_I1)*o_2*xi_2;
        x = @(t) (1/2).*A * [ 
            u_1(t, c_1(1), c_1(2)) ; 
            v_3(t, c_21, c_22) 
        ];
    elseif xi_1  > 1 && xi_2  > 1
        C = (1/(l_2m - l_2p)) * [
                 l_2m, -1;
                -l_2p,  1
            ];
        B =   [        1, 0;
                o_1*xi_1, 1];
        c_1 = B * [x_I1 + x_I2; v_I1 + v_I2];
        c_2 = C * [x_I1 - x_I2; v_I1 - v_I2];
        x = @(t) (1/2).*A * [ 
            u_1(t, c_1(1), c_1(2)) ; 
            v_2(t, c_2(1), c_2(2)) 
        ];
    else
        error("Imposible xi_1, xi_2 pair");
    end
    
    X = x(t);    
end




















