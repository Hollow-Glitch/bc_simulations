function x = analytic_simple_osc_damped(t, x_0, v_0, m, k, c, create_plot, print_plot, name_sys_const)
% x = analytic_simple_osc(t, x_0, v_0, m, k, create_plot)
% 
% arguments:
% x_0 ... initial displacement   |   x_0 in (-infty, 0) U (0, infty)
% v_0 ... initial velocity       |   v_0 in (-infty, 0) U (0, infty)
% m   ... mass                   |   m   in (0, infty)
% k   ... spring constant        |   k   in (0, infty)
% c   ... damping coeficient     |   c   in (0, infty)
%
% returns:
% x   ... single number if t is a number, or a list of 'n' numbers if 
%         t is a list of length 'n'
% 
% additional:
% create_plot    ... set to 'true' if you want to also create plot
% print_plot     ... set to 'true' if you want to also print the plot
% name_sys_const ... set to 'true' if you want to use system constants m,k,c,... in the output file name


    %% safe checks
    if x_0 == 0
        disp("Without initial displacement the system doesn't perform any motion");
        return;
    end
    if ~(k > 0)
        disp("Error not sattisfied: k>0");
        return;
    end
    if ~(c > 0)
        disp("Error not sattisfied: c>0");
        return;
    end
    if ~(m > 0)
        disp("Error not sattisfied: m>0");
        return;
    end

    %% computations
    % let us replace in this function some symbols with letters:
    % - 'omega' with 'f' since it is the angular [f]requency
    f = k/m;
    xi = c/(2*sqrt(m*k));
    
    if xi < 1
        x = under_damped(t, f, xi, x_0, v_0);
        what_damped = "under_damped";
        disp(what_damped);
    elseif xi == 1
        x = critically_damped(t, f, xi, x_0, v_0);
        what_damped = "critically_damped";
        disp(what_damped);
    else % xi > 1
        x = over_damped(t, f, xi, x_0, v_0);
        what_damped = "over_damped";
        disp(what_damped);
    end

    %% additional
    if create_plot
        if print_plot
            ploting
        end
        
        plot(t, x);
        hold on;
        xlabel('t');
        ylabel('x(t)');
        xline(0);
        yline(0);
        
        if print_plot
            if name_sys_const
                name = sprintf('osc_n=1_analytic_%s__x_I=%.2f_v_I=%.2f_k=%.2f_c=%.2f_m=%.2f', ...
                    what_damped, x_0, v_0, k, c, m);
            else
                name = sprintf('osc_n=1_analytic_%s__x_I=%.2f_v_I=%.2f_f=%.2f_xi=%.2f_m=%.2f', ...
                    what_damped, x_0, v_0, f, xi, m);
            end
            smart_plot_print(name);
        end
    end
end


function x = under_damped(t, f, xi, x_0, v_0)
    a = -f*xi;
    b = f*sqrt(1-xi^2);
    c = [1 0 ; -a 1]*[x_0; v_0];
    
    x = c(1).*exp(a.*t).*cos(b.*t) + c(2).*exp(a.*t).*sin(b.*t);
end


function x = over_damped(t, f, xi, x_0, v_0)
    l1 = -xi*f + f*sqrt(xi^2-1);
    l2 = -xi*f - f*sqrt(xi^2-1);
    
    c = (1/(l2-l1))*[l2 -1 ; -l1 1]*[x_0 ; v_0];
    
    x = c(1).*exp(l1.*t) + c(2).*exp(l2.*t);
end


function x = critically_damped(t, f, xi, x_0, v_0)
    l = -xi*f;
    c = [x_0; v_0+x_0*f*xi];
    
    x = c(1).*exp(l.*t) + c(2).*t.*exp(l.*t);
end