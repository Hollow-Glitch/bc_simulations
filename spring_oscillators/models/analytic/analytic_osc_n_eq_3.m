function Y = analytic_osc_n_eq_3(t, x_I1, x_I2, x_I3, k, m, create_plot, print_plot)
%function Y = analytic_osc_n_eq_2(t, x_I1, x_I2, k_1, k_2, m, create_plot, print_plot, name_sys_const)
%ANALYTIC_OSC_N_EQ_2 

    %% safe checks
    if (x_I1 == 0) & (x_I2 == 0) & (x_I3 ==0)
        disp("Without initial displacement the system doesn't perform any motion");
        return;
    end
    if ~(k > 0)
        disp("Error not sattisfied: k>0");
        return;
    end
    if ~(m > 0)
        disp("Error not sattisfied: m>0");
        return;
    end
    
    %% computations
    %f = sqrt(k / m);
    os = k/m; % omega squared

    c_1 = (-2*x_I1 + 2*x_I3) / 4;
    c_3 = (x_I1 - sqrt(2)*x_I2 + x_I3) / 4;
    c_5 = (x_I1 + sqrt(2)*x_I2 + x_I3) / 4;
    
    A = [-1,        1,          1;
          0, -sqrt(2),    sqrt(2);
          1,        1,          1
        ];
    
    x_f = @(t) A*[ c_1*cos(sqrt(2)*os*t);
                   c_3*cos(sqrt(2+sqrt(2))*os*t);
                   c_5*cos(sqrt(2-sqrt(2))*os*t)
                  ];
    x = zeros(3, length(t));
    for i = 1:length(t)
        x(:,i) = x_f(t(i));
    end
    
    % velocity
    dx_f = @(t) A*[ - c_1*sin(sqrt(2)*os         *t) *sqrt(2)*os;
                   - c_3*sin(sqrt(2+sqrt(2))*os *t) *sqrt(2+sqrt(2))*os;
                   - c_5*sin(sqrt(2-sqrt(2))*os *t) *sqrt(2-sqrt(2))*os
                  ];
    dx = zeros(3, length(t));
    for i = 1:length(t)
        dx(:,i) = dx_f(t(i));
    end
    
    Y = [x;dx];
    
    %% additional
    if create_plot
        if print_plot
            ploting
        end
        
        hold on;
        p1 = plot(t, x(1,:));
        p2 = plot(t, x(2,:));
        p3 = plot(t, x(3,:));
        %ylim([min(min([x_1, x_2]))*1.25, max(max([x_1, x_2]))*1.25]);
        legend([p1,p2,p3], 'x_1(t)', 'x_2(t)', 'x_3(t)', 'AutoUpdate', 'off');
        xlabel('t');
        ylabel('x(t)');
        xline(0);
        yline(0);
        
        if print_plot
%             if name_sys_const
                name = sprintf('osc_n=3_analytic__c1=%f_c3=%f_c5=%f__x1=%d_x2=%d_x3=%d__k=%d_m=%d', ...
                               c_1, c_3, c_5, x_I1, x_I2, x_I3, k, m);
%             else
%                 name = sprintf('osc_n=1_analytic_%s__x_I=%.2f_v_I=%.2f_f=%.2f_xi=%.2f_m=%.2f', ...
%                     what_damped, x_0, v_0, f, xi, m);
%             end
            smart_plot_print(name);
        end
    end
end