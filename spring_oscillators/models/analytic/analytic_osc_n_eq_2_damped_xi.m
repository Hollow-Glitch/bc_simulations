
c = 1;
m = 1;
% k = 1;

xi_1 = @(k) c ./ (2.*sqrt(m.*k));
xi_2 = @(k) c ./ (2.*sqrt(3.*m.*k));

k = 0:0.01:0.5;

k_when_xi_1_is_1 = (1/m)*(c/2)^2
k_when_xi_2_is_1 = (1/m)*(c/(2*sqrt(3)))^2

hold on;
ylim([0, 2.5]);

plot(k, xi_1(k), 'DisplayName','\xi_1(k)');
plot(k, xi_2(k), 'DisplayName','\xi_2(k)');
legend('AutoUpdate', 'off');
yline(1);
xline(k_when_xi_1_is_1);
xline(k_when_xi_2_is_1);


name = sprintf('osc_n=2_damped_xi_analytic__c=%f_m=%f__k(xi1=1)=%f__k(xi2=1)=%f', c, m, k_when_xi_1_is_1, k_when_xi_2_is_1);
smart_plot_print(name);