function [supEigVec, eigValues] = analytic_osc_n_eq_N_eigen(N, k,m, eigCol1, eigCol2)
% draws/plots 2 given EIGENVECTORS 

    if N<0 | k<0 | m<0
        disp("Error one of the following conditions were not satisfied: N<0, k<0, m<0");
        return;
    end
    if N < eigCol1 | eigCol1 < 0
        disp("Error one of the following conditions were not satisfied: N < eigCol1, eigCol1 < 0");
    end
    if N < eigCol2 | eigCol2 < 0
        disp("Error one of the following conditions were not satisfied: N < eigCol2, eigCol2 < 0");
    end

    os = k/m;   % omega squared

    % setup A
    A = zeros(N);
    A(2:N+1:end-N) = -os;
    A = A';
    A(2:N+1:end-N) = -os;
    A(1:N+1:end) = 2.*os;

    % eigen values, eigen vectors of A
    [eigVec,val] = eig(A);
    eigValues = diag(val);
    supEigVec = [zeros(1,N);eigVec;zeros(1,N)];    % suplemented eig vec - added the walls
    
    % drawing
    x = 0:N+1;
    hold on;
    plot(x, supEigVec(:,eigCol1), '-o');
    plot(x, supEigVec(:,eigCol2), '--o');

    legend(sprintf('%d. eigen vector',eigCol1), ...
           sprintf('%d. eigen vector',eigCol2)  )
    xlabel('i');
    ylabel('i-th element of the n-th eigenvector');
    xlim([0,55]);
    ylim([-0.25,0.25]);
end

