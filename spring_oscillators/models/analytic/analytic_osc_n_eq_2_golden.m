function x = analytic_osc_n_eq_2_golden(t, x_I1, x_I2, k, m, create_plot, print_plot)
%function x = analytic_osc_n_eq_2(t, x_I1, x_I2, k_1, k_2, m, create_plot, print_plot, name_sys_const)
%ANALYTIC_OSC_N_EQ_2 

    %% safe checks
    if (x_I1 == 0) & (x_I2 == 0)
        disp("Without initial displacement the system doesn't perform any motion");
        return;
    end
    if ~(k > 0)
        disp("Error not sattisfied: k>0");
        return;
    end
    if ~(m > 0)
        disp("Error not sattisfied: m>0");
        return;
    end
    
    %% computations

    f = sqrt(k / m);        % omega
    p = (sqrt(5) +1) /2;    % varphi

    c_1 = (p*x_I1 - x_I2) / (2*p-1);
    c_3 = ((p-1)*x_I1 + x_I2) / (2*p-1);
    
    x_1_f = @(t)( c_1 .* cos(f .*p .* t) + c_3 .* cos(f .* (p-1) .* t) );
    x_2_f = @(t)( c_1 .* (1-p) .* cos(f .*p .* t) + c_3 .* p .* cos(f .* (p-1) .* t) );
    
    x_1 = x_1_f(t);
    x_2 = x_2_f(t);
    x = [ x_1; x_2 ];
    
    %% additional
    if create_plot
        if print_plot
            ploting
        end
        
        hold on;
%         plot(t, x(1,:), 'b-');
%         plot(t, x(2,:), 'r--');
        p1 = plot(t, x_1, '-');
        p2 = plot(t, x_2, '-');
        ylim([min(min([x_1, x_2]))*1.25, max(max([x_1, x_2]))*1.25]);
        legend([p1,p2],'x_1(t)', 'x_2(t)', 'AutoUpdate', 'off');
        xlabel('t');
        ylabel('x(t)');
        xline(0);
        yline(0);
        
        if print_plot
%             if name_sys_const
                name = sprintf('osc_n=2_golden_analytic__x_I1=%.2f_x_I2=%.2f_k=%.2f_m=%.2f', ...
                    x_I1, x_I2, k, m);
%             else
%                 name = sprintf('osc_n=1_analytic_%s__x_I=%.2f_v_I=%.2f_f=%.2f_xi=%.2f_m=%.2f', ...
%                     what_damped, x_0, v_0, f, xi, m);
%             end
            smart_plot_print(name);
        end
    end
end