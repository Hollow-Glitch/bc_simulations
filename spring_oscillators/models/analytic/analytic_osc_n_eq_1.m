function Y = analytic_osc_n_eq_1(t, x_0, v_0, m, k, create_plot, print_plot)
% Y = analytic_simple_osc(t, x_0, v_0, m, k, create_plot)
% 
% arguments:
% x_0 ... initial displacement   |   x_0 in (-infty, 0) U (0, infty)
% v_0 ... initial velocity       |   v_0 in (-infty, 0) U (0, infty)
% m   ... mass                   |   m   in (0, infty)
% k   ... spring constant        |   k   in (0, infty)
%
% returns:
% Y = [x;v]
% x   ... single number if t is a number, or a list of 'n' numbers if 
%         t is a list of length 'n'
% 
% additional:
% create_plot ... set to 'true' if you want to also create plot
% print_plot  ... set to 'true' if you want to also print the plot


    %% safe checks
    if x_0 == 0
        disp("Without initial displacement the system doesn't perform any motion");
        return;
    end
    if ~(k > 0)
        disp("Error not sattisfied: k>0");
        return;
    end
    if ~(m > 0)
        disp("Error not sattisfied: m>0");
        return;
    end

    %% computations
    % let us replace in this function some symbols with letters:
    % - 'omega' with 'f' since it is the angular [f]requency
    % - 'phi' with 'p' since it is the [p]hase
    f = k/m;
    A = sqrt(x_0^2 + (v_0^2)/(f^2));
    p = atan((-v_0)/(f*x_0));


    simp_osc = @(t)(A*cos(f*t+p));
    x = simp_osc(t);
    dsimp_osc = @(t) - A * sin(f*t+p) * f;
    v = dsimp_osc(t);
    
    Y = [x;v];

    %% additional
    if create_plot
        if print_plot
            ploting
        end
        
        plot(t, x);
        hold on;
        xlabel('t');
        ylabel('x(t)');
        
        if print_plot
            smart_plot_print(...
                sprintf('osc_n=1_analytic__x_I=%.1f_v_I=%.1f_k=%.1f_m=%.1f', ...
                        x_0, v_0, k, m)...
            );
        end
    end
end

