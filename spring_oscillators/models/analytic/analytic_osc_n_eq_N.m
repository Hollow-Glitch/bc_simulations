

function Y = analytic_osc_n_eq_N(t, k, m, x_I)
% analytic_osc_n_eq_N
%
% Y = analytic_osc_n_eq_N(t, k, m, x_I)
%
% k    ...  spring constant of every spring
% m    ...  mass of every mass
% x_I  ...  column vector of the initial displacements
%
% The number of coupled oscillators, i.e. N, is equal to the number
% of elements of the IC vector, i.e. x_I
    
    x_I = x_I(:);
    N = numel(x_I);
    %% safe checks
    if numel(x_I) ~= N
        disp("element count of the initial displacement vector x_I doesn't equal number of masses N");
        return;
    end
    if max(abs(x_I)) == 0
        disp("Without initial displacement the system doesn't perform any motion");
        return;
    end
    if ~(k > 0)
        disp("Error not sattisfied: k>0");
        return;
    end
    if ~(m > 0)
        disp("Error not sattisfied: m>0");
        return;
    end
    
    %% computations
    omega = sqrt(k / m);
    
    % phi, Omega
    phi = @(n, N) (n .* pi) ./ (N + 1);
    O = @(n, N) 2.*omega.*sin(phi(n, N)/2);
    
%     % construct M
%     Msin = @(rowIndex, colIndex, N) sin(rowIndex*phi(colIndex, N));
%     M = zeros(N ,N);
%     for rowIndex = 1:N
%         for colIndex = 1:N
%             M(rowIndex, colIndex) = Msin(rowIndex, colIndex, N);
%         end
%     end
    % has been separated out into its own function file
    M = analytic_osc_n_eq_N_construct_M(N);
    
    % c11, c12, ..., c1N
    c_1 = inv(M) * x_I;
    
    s = @(t) c_1 .* cos(   O((1:N)',N)   .* t);
    x = @(t) M * s(t);
    X = x(t);
    
    s_2 = @(t) -c_1 .* sin(   O((1:N)',N)   .* t) .* O((1:N)',N);
    dx = @(t) M * s_2(t);
    dX = dx(t);
    
    Y = [X; dX];
    
%     x_I
%     c_1
%     M
%     s
    %disp("cos(   O((1:N)',N)   .* t)' ="); disp(cos(   O((1:N)',N)   .* t)');
    %disp("s(t)' ="); disp(s(t)');
    disp("inv(M) ="); disp(inv(M));
    
    disp("X' ="); disp(X');
    
end

