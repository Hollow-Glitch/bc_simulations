classdef model_class_osc_n_eq_2
%TWO_COUPLED_OSCILLATORS_DAMPED
%     k1    ... spring constant of the outer springs
%     k2    ...  spring constant of the middle spring
%     m     ...  mass of the massese
%     x_i1  ...  initial displacement of the first mass
%     v_i1  ...  initial velocity of the first mass
%     x_i2  ...  initial displacement of the second mass
%     v_i2  ...  initial velocity of the second mass
    
    properties
        k1   % spring constant of the outer springs
        k2   % spring constant of the middle spring
        m    % mass of the massese
        x_i1 % initial displacement of the first mass
        v_i1 % initial velocity of the first mass
        x_i2 % initial displacement of the second mass
        v_i2 % initial velocity of the second mass
        
        t
        y
        x1 % displacement of the 1. mass
        v1 % velocity of the 1. mass
        x2 % displacement of the 2. mass
        v2 % velocity of the 2. mass
        w1
        w2
        eqp1 % equilibrium point of the 1. mass
        eqp2 % equilibrium point of the 2. mass
        
        spring
    end
    
    methods
        function obj = model_class_osc_n_eq_2(...
                k1, k2, m, x_i1, v_i1, x_i2, v_i2)
            if nargin ~= 7
                disp("nargin~=7");
                return
            end
            
            obj.k1   = k1;
            obj.k2   = k2;
            obj.m    = m; 
            obj.x_i1 = x_i1; 
            obj.v_i1 = v_i1; 
            obj.x_i2 = x_i2; 
            obj.v_i2 = v_i2;

            obj.spring = Spring(0.3, 10);
            
            obj.w1 = [0 1];
            obj.w2 = [3 1];
        end
        
        function obj = solve(obj)
            IC = [
                obj.x_i1;
                obj.v_i1;
                obj.x_i2;
                obj.v_i2
                ];
            [obj.t, obj.y] = ode45(...
                @(t, y) model_osc_n_eq_2(t, y, obj.k1, obj.k2, obj.m), [0 20], IC ...
            );
            % t = t;
            obj.x1 = obj.y(:, 1); % displacement of the first mass
            obj.v1 = obj.y(:, 2); % velocity of the first mass
            obj.x2 = obj.y(:, 3); % displacement of the second mass
            obj.v2 = obj.y(:, 4); % velocity of the second mass

            sl_third = norm(obj.w2-obj.w1) /3; % sl(SystemLength)
            obj.eqp1 = obj.w1 + [sl_third, 0];
            obj.eqp2 = obj.w1 + [sl_third * 2, 0];
        end
        
        function pointMass(~, C) 
            rectangle('Position',[C(1)-0.05, C(2)-0.05, .1, .1],'FaceColor',[0 0 0]);
        end
        
        function draw(obj, i)
            title(obj.t(i));

            xline(obj.w1(1));
            xline(obj.eqp1(1));
            xline(obj.eqp2(1));
            xline(obj.w2(1));

            m1pos = obj.eqp1 + [obj.x1(i), 0];
            m2pos = obj.eqp2 + [obj.x2(i), 0];
            
            [springX, springY] = obj.spring.getSpr(obj.w1, m1pos);
            plot(springX,springY, 'color', 'blue');
            obj.pointMass(m1pos);
            [springX, springY] = obj.spring.getSpr(m1pos, m2pos);
            plot(springX,springY, 'color', 'blue');
            obj.pointMass(m2pos);
            [springX, springY] = obj.spring.getSpr(m2pos, obj.w2);
            plot(springX,springY, 'color', 'blue');
        end
    end
end
