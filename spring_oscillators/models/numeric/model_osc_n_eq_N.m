function dY = model_osc_n_eq_N(t, Y, n, k, m)
    os = k / m;

    y_j1 = Y(1:2:2*n);
    y_j2 = Y(2:2:2*n);

    dy_j1 = zeros(n,1);
    dy_j2 = zeros(n,1);
    
    dy_j1(1) = y_j2(1);
    dy_j2(1) = -2*os*y_j1(1) + os*y_j1(2);
    
    for i = 2:n-1
        dy_j1(i) = y_j2(i);
        dy_j2(i) = os*y_j1(i-1) - 2*os*y_j1(i) + os*y_j1(i+1);
    end
    
    dy_j1(n) = y_j2(n);
    dy_j2(n) = os*y_j1(n-1) - 2*os*y_j1(n);
    
    dY = zeros(n*2,1);
    dY(1:2:2*n) = dy_j1;
    dY(2:2:2*n) = dy_j2;
end