

function n_eq_1_damped()
% n_eq_1_damped - Animation of the: Simple harmonic oscillator case (with damping)

    k = 3;   % spring constant
    c = 0.7; % damping coeficient
    m = 1;   % oscillator mass
    x_0 = 1; % initial displacement
    v_0 = 0; % initial velocity
    IC = [   % initial conditions
        x_0;
        v_0 
        ];
    
    [t, y] = ode45(@(t, y) model_osc_n_eq_1_damped(t, y, k, c, m), [0 15], IC);
    t = t;       % time
    x = y(:, 1); % displacement
    v = y(:, 2); % velocity
    
    wall = [0 1];
    massWidth = 1;
    spring = Spring(0.3, 10);
    
    % limit subplots
    subplot(3,1,1);
    ylim([min(x) max(x)]);
    xlim([min(t) max(t)]);
    hold on;
    
    subplot(3,1,2);
    ylim([min(v) max(v)]);
    xlim([min(t) max(t)]);
    hold on;
    
    subplot(3,1,3);
    xlim([0 5]);
    ylim([0 2]);
    
    x_max = max(x);
    j = 1;
    for i = 1:length(t)
        subplot(3,1,3);
        cla;
        
        % displacement plot
        subplot(3,1,1); 
        cla;
        title("displacement");
        plot(t(1:i), x(1:i));
        yline(0);
        
        % velocity plot
        subplot(3,1,2); 
        cla;
        title("velocity");
        plot(t(1:i), v(1:i));    
        yline(0);
        
        % animation
        subplot(3,1,3);
        hold on;
        title("movement visualization");
        displacedTo = wall + [x(i) + x_max*2, 0];
        [springX, springY] = spring.getSpr(wall, displacedTo);
        plot(springX,springY, 'color', 'blue');
        mass(displacedTo(1)+massWidth/2, wall(2), massWidth, 1, "m="+m+"kg");
        
        j = i;
        drawnow
        % pause(0.01);
    end
end

function mass(xCenter, yCenter, width, height, massText)
    rectangle('Position', [xCenter-width/2, yCenter-height/2, width, height]);
    text(xCenter, yCenter, massText, ...
        'HorizontalAlignment', 'center', ...
        'VerticalAlignment', 'middle');
end