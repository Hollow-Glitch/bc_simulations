

function n_eq_3()
% n_eq_3 - Animation of the: Three Coupled Oscillators case (no damping)

    k = 1;
    m = 1;
    x_i1 = 0.5;
    v_i1 = 0;
    x_i2 = 0;
    v_i2 = 0;
    x_i3 = 0;
    v_i3 = 0;
    IC = [
        x_i1;
        v_i1;
        x_i2;
        v_i2;
        x_i3;
        v_i3
        ];
    [t, Y] = ode45(@(t,y) model_osc_n_eq_3(t, y, k, m), [0 10], IC);
    t = t;
    x1 = Y(:, 1);
    v1 = Y(:, 2);
    x2 = Y(:, 3);
    v2 = Y(:, 4);
    x3 = Y(:, 5);
    v3 = Y(:, 6);
    

    spring = Spring(0.3, 10);
    
    w1 = [0 1];
    w2 = [4 1];
    sl_third = norm(w2-w1) /4;
    eqp1 = w1 + [sl_third 0];
    eqp2 = w1 + [sl_third 0] * 2;
    eqp3 = w1 + [sl_third 0] * 3;
    
    % ------------------------------------------------------------
    subp = @(a) subplot(3, 3, a);
    animplot = @() subp([7 8 9]);
    minT = min(t);
    maxT = max(t);
    
    subp(1);
    xlim([minT, maxT]);
    ylim([min(x1), max(x1)]);
    plot(t, x1);
    yline(0);
    title("1. mass displ.");
    hold on;
    
    subp(2);
    xlim([minT, maxT]);
    ylim([min(x2), max(x2)]);
    plot(t, x2);
    yline(0);
    title("2. mass displ.");
    hold on;
    
    subp(3);
    xlim([minT, maxT]);
    ylim([min(x3), max(x3)]);
    plot(t, x3);
    yline(0);
    title("3. mass displ.");
    hold on;
    
    subp(4);
    xlim([minT, maxT]);
    ylim([min(v1), max(v1)]);
    plot(t, v1);
    yline(0);
    title("1. mass velocity");
    hold on;
    
    subp(5);
    xlim([minT, maxT]);
    ylim([min(v2), max(v2)]);
    plot(t, v2);
    yline(0);
    title("2. mass velocity");
    hold on;
    
    subp(6);
    xlim([minT, maxT]);
    ylim([min(v3), max(v3)]);
    plot(t, v3);
    yline(0);
    title("3. mass velocity");
    hold on;
    
    animplot();
    xlim([0 4]);
    ylim([0 2]);
    hold on;
    % ------------------------------------------------------------
    
    pointMass = @(C) rectangle('Position',[C(1)-0.05, C(2)-0.05, .1, .1],'FaceColor',[0 0 0]);
    for i = 1:length(t)
        animplot();
        cla;
        
        xline(w1(1));
        xline(eqp1(1));
        xline(eqp2(1));
        xline(eqp3(1));
        xline(w2(1));
        
        m1pos = eqp1 + [x1(i) 0];
        m2pos = eqp2 + [x2(i) 0];
        m3pos = eqp3 + [x3(i) 0];
        
        [springX, springY] = spring.getSpr(w1, m1pos);
        plot(springX,springY, 'color', 'blue');
        
        pointMass(m1pos);
        [springX, springY] = spring.getSpr(m1pos, m2pos);
        plot(springX,springY, 'color', 'blue');
        
        pointMass(m2pos);
        [springX, springY] = spring.getSpr(m2pos, m3pos);
        plot(springX,springY, 'color', 'blue');
        
        pointMass(m3pos);
        [springX, springY] = spring.getSpr(m3pos, w2);
        plot(springX,springY, 'color', 'blue');

        
        drawnow
        % pause(0.01);
    end
end