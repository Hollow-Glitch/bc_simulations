

function n_eq_N()
% n_eq_N - Animation of the: N Coupled Oscillators case (no damping)

    n = 10;  % number of masses
    k = 1;   % k spring constant of each spring
    m = 1;   % mass of each mass
    num_of_coils = 3; % number of spring coils

    x_Ij = zeros(n, 1);
    v_Ij = zeros(n, 1);
    
    x_Ij(1) = -0.9;
    %v_Ij(1) = 0.5;
    
    IC = [ x_Ij(:) v_Ij(:) ] .';
    IC = IC(:);
    [t, Y] = ode45(@(t, y) model_osc_n_eq_N(t, y, n, k, m), [0 15], IC);
    t = t;
    x_j = Y(:, 1:2:2*n);
    v_j = Y(:, 2:2:2*n);
    
    spring = Spring(0.3, num_of_coils);
    
    w1 = [0, 1];
    w2 = [n+1, 1];
    osc_spacing = norm(w2-w1) / (n+1);
    eq_j = zeros(n, 2);
    for i = 1:n
        eq_j(i, :) = w1 + [osc_spacing, 0] * i;
    end
    
    pointMass = @(C) rectangle('Position',[C(1)-0.05, C(2)-0.05, .1, .1],'FaceColor',[0 0 0]);
    m_j_pos = zeros(n, 2);
    
    ylim([0,2]);
    xlim([w1(1), w2(1)]);
    hold on;
    for i = 1:length(t)
        cla;
        
        xline(w1(1));
        for j = 1:n
            xline(eq_j(j, 1));
            %disp("eq_j(" + j + ",:) = [" + eq_j(j,1) + ", " + eq_j(j,2) + "]");
            m_j_pos(j,:) = eq_j(j,:) + [x_j(i, j), 0];
            %disp(m_j_pos(j,:) + " = " + eq_j(j,:) + " + " + [x_j(i), 0]);
        end
        xline(w2(1));
        
        [springX, springY] = spring.getSpr(w1, m_j_pos(1,:));
        plot(springX, springY, 'color', 'blue');
        for j = 1:(n-1)
            pointMass(m_j_pos(j,:));
            [springX, springY] = spring.getSpr(m_j_pos(j,:), m_j_pos(j+1,:));
            plot(springX, springY, 'color', 'blue');
        end
        pointMass(m_j_pos(n,:));
        [springX, springY] = spring.getSpr(m_j_pos(n,:), w2);
        plot(springX, springY, 'color', 'blue');
        
        drawnow;
%         pause(0.01);
    end
end