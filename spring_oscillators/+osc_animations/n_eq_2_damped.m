

function n_eq_2_damped()
% n_eq_2_damped - Animation of the: Two Coupled Oscillators case (with damping)

    disp("two_coupled_oscillators_damped_with_class");
    tco = model_class_osc_n_eq_2_damped(0.1, 2, 1, 1, -0.5, 0, 0, 0);
    tco = tco.solve();
    subp = @(a) subplot(3, 2, a);
    animplot = @() subp([5 6]);
    
    subp(1);
    xlim([min(tco.t), max(tco.t)]);
    ylim([min(tco.x1), max(tco.x1)]);
    hold on;
    
    subp(2);
    xlim([min(tco.t), max(tco.t)]);
    ylim([min(tco.x2), max(tco.x2)]);
    hold on;
    
    subp(3);
    xlim([min(tco.t), max(tco.t)]);
    ylim([min(tco.v1), max(tco.v1)]);
    hold on;
    
    subp(4);
    xlim([min(tco.t), max(tco.t)]);
    ylim([min(tco.v2), max(tco.v2)]);
    hold on;
    
    animplot();
    xlim([0, 3]);
    ylim([0, 2]);
    hold on;
    
    j = 1;
    for i = 1:length(tco.t)
        cla;
        
        subp(1);
        cla;
        plot(tco.t(1:i), tco.x1(1:i));
        yline(0);
        title("1. mass displ.");
        
        subp(2);
        cla;
        plot(tco.t(1:i), tco.x2(1:i));
        yline(0);
        title("2. mass displ.");

        
        subp(3);
        cla;
        plot(tco.t(1:i), tco.v1(1:i));
        yline(0);
        title("1. mass velocity");

        
        subp(4);
        cla;
        plot(tco.t(1:i), tco.v2(1:i));
        yline(0);
        title("2. mass velocity");
        
        animplot();
        tco.draw(i);
        title("animation");
        
%         pause(0.01);
        drawnow;
        j = i;
    end
end
