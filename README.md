
# Modeling and simulation of multidegree-of-freedom systems

In this repository a matlab project can be found, which is the companion project
for my bachelors thesis, which is about:

- \(n\)-coupled mass spring systems
- \(n\)-linked pendulum systems

This project contains:

- implementation of the numerical and analytical models
- the image generators which have been used to generate the various charts
  in the thesis
- animation programs for all modeled situations

The primary result of this project are the animation programs,
while by the rest of the project allows the reader to verify the correctness
of every chart found in the thesis.

## Setup

The recommended Matlab version is: `R2019a Update 5`.

The project file is called: `Bc_simulations.prj`

For a general overview on project management in matlab, see:
<https://www.mathworks.com/help/simulink/project-management.html?s_tid=CRUX_lftnav>

## Project structure

The `startup.m` file sets `format compact;`, if this is not desired by you,
change it accordingly.

The following block explains the directory structure.

```{}
.
├── pendulums
│   ├── models ..................... implemented pendulum models
│   │   ├── analytic
│   │   └── numeric
│   ├── +pend_animations ........... pendulum animations based on ODE45
│   ├── +pend_image_generators ..... code used for generating pendulum img files
│   └── +pend_ploters
├── spring_oscillators
│   ├── +analytic_osc_animations ... oscillator animations based on analytic solutions
│   ├── models ..................... implemented oscillator models
│   │   ├── analytic
│   │   └── numeric
│   ├── +osc_animations ............ oscillator animations based on ODE45
│   ├── +osc_image_generators ...... code used for generating oscillator img files
│   └── +wave ...................... code regarding the wave equation
│       ├── +init_displ_funcs
│       └── +ploters
├── utilities ...................... code primarily used for automation
├── Bc_simulations.prj
└── README.md ...................... this readme
```

## Running animations

If you wish to start one of the implemented animations,
open the matlab command line, and specify the package name
(either: pend_animations, analytic_osc_animations or osc_animations),
and the name of the situation.

Examples - to run the:

- Simple harmonic oscillator animation without damping, execute:
  `analytic_osc_animations.n_eq_1`
- Simple harmonic oscillator animation with damping, execute:
  `analytic_osc_animations.n_eq_1_damped`
- N coupled oscillator animation, execute:
  `analytic_osc_animations.n_eq_N`
- Spring pendulum animation, execute:
  `pend_animations.n_eq_1_spring`
- Double pendulum animation, execute:
  `pend_animations.n_eq_2`

